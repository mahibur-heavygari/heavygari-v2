<?php

return [
    'user_service_host' => env('USER_SERVICE_HOST') ?: '',
    'full_booking_service_host' => env('FULL_BOOKING_SERVICE_HOST') ?: ''
];
