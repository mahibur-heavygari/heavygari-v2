<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v2/authentication', 'middleware' => 'cors'], function() {

	Route::post('/login', 'AuthController@login');
	Route::get('/logout',  ['uses'=>'AuthController@logout', 'middleware'=>'auth:api']);
});


Route::group(['prefix' => 'v2/cms', 'middleware' => 'cors'], function() {
	
	Route::group(['prefix' => 'panel', 'middleware' => 'auth:api'], function () {
		
		Route::group(['middleware' => 'complete-profile:admin'], function() {
			
			Route::get('/profile', ['uses'=>'CMS\User\ProfileController@show', 'middleware' => 'permission:admin_general']);

		    Route::get('/users/admins/{id}', ['uses'=>'CMS\User\AdminController@show', 'middleware' => 'permission:admin_user_management']);
		    Route::post('/users/admins/store', ['uses'=>'CMS\User\AdminController@store', 'middleware' => 'permission:admin_user_management']);
		    
		    Route::get('/users/customers/{id}', ['uses'=>'CMS\User\CustomerController@show', 'middleware' => 'permission:admin_user_management']);
		    Route::post('/users/customers/store', ['uses'=>'CMS\User\CustomerController@store', 'middleware' => 'permission:admin_user_management']);
		    Route::post('/users/customers/{id}/profile-update', ['uses'=>'CMS\User\CustomerController@updateProfile', 'middleware' => 'permission:admin_user_management']);
		    
		    Route::get('/users/owners/{id}', ['uses'=>'CMS\User\OwnerController@show', 'middleware' => 'permission:admin_user_management']);
		    Route::post('/users/owners/store', ['uses'=>'CMS\User\OwnerController@store', 'middleware' => 'permission:admin_user_management']);
		    Route::post('/users/owners/{id}/profile-complete', ['uses'=>'CMS\User\OwnerController@completeProfile', 'middleware' => 'permission:admin_user_management']);
		    Route::post('/users/owners/{id}/profile-update', ['uses'=>'CMS\User\OwnerController@updateProfile', 'middleware' => 'permission:admin_user_management']);
		
		});
	});
});


Route::group(['prefix' => 'v2/customer', 'middleware' => 'cors'], function() {
	
	Route::group(['prefix' => '/authentication'], function() {
		
		/* Check phone number whether it exists in server or not. 
		1. If phone does not exist, the server generates a pin and sends to the phone number. The server will redirect to step-two-pin route.
		1(i). If the pin matchs, the server will redirect to step-three-sign-up route.
		2. If phone exists in the customer panel, server returns a response with error. Then server will redirect to login route.
		3. If phone exists in another panel, server returns a response with message. The server will redirect to step-two-password route. If the user provides a valid password, the server will make the user logged in providing a bearer token.
		*/

		//Routes for registration
		Route::post('/register/step-one-check-phone', 'Customer\RegistrationController@checkPhone');
		Route::post('/register/step-two-pin', 'Customer\RegistrationController@matchPin');
		Route::post('/register/step-two-password', 'Customer\RegistrationController@matchPassword');
		Route::post('/register/step-three-sign-up', 'Customer\RegistrationController@register');
		
		//Routes for forget password
		Route::post('/register/forget-password/send-pin', 'Customer\RegistrationController@sendPinAtForgetPassword');
		Route::post('/register/forget-password/match-pin', 'Customer\RegistrationController@matchPinAtForgetPassword');
	});

	Route::group(['prefix' => '/panel', 'middleware' => 'auth:api'], function() {
		Route::group(['middleware' => 'complete-profile:customer'], function() {
			Route::get('/profile', ['uses'=>'Customer\ProfileController@show', 'middleware' => 'permission:customer_general']);
			Route::post('/profile-update', ['uses'=>'Customer\ProfileController@updateProfile', 'middleware' => 'permission:customer_general']);
			
			// bookings
			Route::group(['prefix' => 'bookings'], function() {
				Route::get('/vehicle-types', ['uses'=>'Customer\FullBookingController@getVehicleTypes', 'middleware' => 'permission:customer_booking_management']);
				Route::post('/full/get-cost', ['uses'=>'Customer\FullBookingController@getCost', 'middleware' => 'permission:customer_booking_management']);
				Route::post('/full/create', ['uses'=>'Customer\FullBookingController@create', 'middleware' => 'permission:customer_booking_management']);
			});
		});
	});
});


Route::group(['prefix' => 'v2/owner', 'middleware' => 'cors'], function() {
	
	Route::group(['prefix' => '/authentication'], function() {
		
		Route::post('/register/step-one-check-phone', 'Owner\RegistrationController@checkPhone');
		Route::post('/register/step-two-pin', 'Owner\RegistrationController@matchPin');
		Route::post('/register/step-two-password', 'Owner\RegistrationController@matchPassword');
		Route::post('/register/step-three-sign-up', 'Owner\RegistrationController@register');
	});

	
	Route::group(['prefix' => '/panel', 'middleware' => 'auth:api'], function() {

		Route::post('/profile-complete', 'Owner\ProfileController@completeProfile');

		Route::group(['middleware' => 'complete-profile:owner'], function() {
			Route::get('/profile', ['uses'=>'Owner\ProfileController@show', 'middleware' => 'permission:owner_general']);
			Route::post('/profile-update', ['uses'=>'Owner\ProfileController@updateProfile', 'middleware' => 'permission:owner_general']);
		});
	});
});


