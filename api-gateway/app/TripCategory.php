<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripCategory extends Model
{
    protected $table = 'base_trip_categories';

    protected $fillable = [
        'src_point_id', 'dest_point_id', 'trip_category'
    ];
}
