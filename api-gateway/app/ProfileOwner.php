<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileOwner extends Model
{
    protected $table='base_profile_owners';
}
