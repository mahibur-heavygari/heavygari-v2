<?php

namespace App\Libraries\UserLib\Login;

use Illuminate\Support\Facades\Auth;
use App\User;
use Hash;
use DB;

class LoginManager
{
	public $user_panel = null;
	protected $user = null;
	protected $error;

	public function login( $username, $password, $user_panel )
	{
		$cred_res = $this->checkCredentials( $username, $password );
		if( $cred_res == false )
			return false;

		$block_res = $this->checkIfBlock();
		if( $block_res == false )
			return false;

		$user_active_res = $this->checkUserIfInactive();
		if( $user_active_res == false )
			return false;

		$this->user_panel = $user_panel;
		$role_verify_res = $this->verifyRole();
		if( $role_verify_res == false )
			return false;

		return $this->user->first();
	}

	private function checkCredentials( $username, $password )
	{
		$user = User::where('username', $username)->where('status', '!=', 'deleted');

		if( !$user->exists() ){
	    	$this->error['message'] = 'Incorrect username / password';
			$this->error['type'] = 'incorrect_login';
			return false;
		}else if( ! Hash::check( $password, $user->first()->password ) ){
	    	$this->error['message'] = 'Incorrect username / password';
			$this->error['type'] = 'incorrect_login';
			return false;
		}
		$this->user = $user;
		return true;
	}

	private function checkIfBlock()
	{
		$user = $this->user->first();
		if( $user->status == 'blocked' ){
	    	$this->error['message'] = 'Blocked User';
			$this->error['type'] = 'blocked_user';
			return false;
		}
		return true;
	}

	private function checkUserIfInactive()
	{
		$user = $this->user->first();
		if( $user->status == 'inactive' || $user->status == 'deleted'){
	    	$this->error['message'] = 'Inactive User';
			$this->error['type'] = 'inactive_user';
			return false;
		}
		return true;
	}

	private function verifyRole()
	{
		$user = $this->user->first();
		if($this->user_panel == 'any')
			return true;
		
		$role = $user->roles->where('panel', $this->user_panel)->first();
		if(is_null($role)){
	    	$this->error['message'] = 'Your user panel did not match';
			$this->error['type'] = 'panel_mismatch';
			return false;
		}

		return true;
	}

	public function getPanels()
	{
		$user = $this->user->first();
		$panels = $user->roles->pluck('panel')->toArray();
		return $panels;
	}

    public function complete( $request, $user_panel, $platform, $os, $fcm_token ){
    	Auth::attempt(['username' => request('username'), 'password' => request('password')]);
        $user = Auth::user();
        $personal_access_token_result = $user->createToken($platform);
        $content['token'] =  $personal_access_token_result->accessToken;
        $personal_access_token_result->token->update(['panel' => $user_panel, 'os' => $os, 'fcm' => $fcm_token]);
        $content['status'] = 200;

        if( $platform == 'mobile' ){
        	DB::table('oauth_access_tokens')->where('name', 'mobile')->where('panel', $user_panel)->where('user_id', $user->id)->where('id', '!=', $personal_access_token_result->token->id)->update(['revoked' => '1']);
        }
        
        return compact('content');
    }

	public function getError() {
		return $this->error;
	}
}