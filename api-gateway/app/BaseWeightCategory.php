<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseWeightCategory  extends Model
{
    protected $table='base_weight_categories';

    protected $fillable = [
        'title', 'weight'
    ];
}
