<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseProductCategory  extends Model
{
    protected $table='base_product_categories';

    protected $fillable = [
        'title', 'category'
    ];
}
