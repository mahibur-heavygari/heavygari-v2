<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileVehicle extends Model
{
    protected $table='base_profile_vehicles';
}
