<?php

namespace App\Enumarations;

class RoleWisePermissions
{
    const Role_WISE_PERMISSIONS = array(
        "admin_super" => array(
            'admin_user_management' => 'User Management',
            'admin_booking_management' => 'Booking Management',
            'admin_accounts_management' => 'Accounts Management',
            'admin_analytics_management' => 'Analytics Management',
            'admin_general' => 'General'
        ),
        "admin_support" => array(
            'admin_user_management' => 'User Management',
            'admin_booking_management' => 'Booking Management',
            'admin_general' => 'General',
        ),
        "admin_accountant" => array(
        ),
        "admin_field_team" => array(
        ),
        "owner_regular" => array(
            'owner_vehicle_management' => 'Vehicle Management',
            'owner_driver_management' => 'Driver Management',
            'owner_booking_management' => 'Booking Management',
            'owner_billing_management' => 'Billing Management',
            'owner_general' => 'General',
        ),
        "customer_regular" => array(
            'customer_booking_management' => 'Booking Management',
            'customer_billing_management' => 'Billing Management',
            'customer_general' => 'General',
        ),
        "driver_regular" => array(
        ),
        "vehicle_regular" => array(
        )
    );

}