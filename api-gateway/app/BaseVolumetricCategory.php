<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseVolumetricCategory  extends Model
{
    protected $table='base_volumetric_categories';
    
    protected $fillable = [
        'title', 'height', 'width', 'length', 'volumetric_weight'
    ];
}
