<?php

namespace App\Http\Controllers\CMS\User;

use Illuminate\Http\Request;
use App\Http\Factories\UserFactory;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Requests\Admin\AdminStore;
use App\Http\Controllers\ApiController;
use Validator;

class AdminController extends ApiController
{
    public function show(string $user): JsonResponse
    {
        try {
            $data = (new UserFactory())->getUser($user, 'admins/%d');
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }

    public function store(Request $request): JsonResponse
    {
        $rules = (new AdminStore())->rules();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        try {
            $data = (new UserFactory())->storeUser($request, 'admins/store');
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }
}
