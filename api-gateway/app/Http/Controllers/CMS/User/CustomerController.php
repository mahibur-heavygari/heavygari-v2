<?php

namespace App\Http\Controllers\CMS\User;

use Illuminate\Http\Request;
use App\Http\Factories\UserFactory;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Customer\ProfileFormRequest;
use App\Http\Requests\Customer\CustomerStore;
use Validator;

class CustomerController extends ApiController
{
    public function show(string $user): JsonResponse
    {
        try {
            $data = (new UserFactory())->getUser($user, 'customers/%d');
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }

    public function store(Request $request): JsonResponse
    {
        $rules = (new CustomerStore())->rules();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        try {
            $data = (new UserFactory())->storeUser($request, 'customers/store');
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }

    public function updateProfile(Request $request, $id): JsonResponse
    {
        $rules = (new ProfileFormRequest())->rules();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $customer_id = $id;

        try {
            $data = (new UserFactory())->updateCustomer($request, $customer_id, 'customers/%d/profile-update');
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }
}
