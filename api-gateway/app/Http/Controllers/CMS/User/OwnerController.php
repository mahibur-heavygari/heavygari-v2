<?php

namespace App\Http\Controllers\CMS\User;

use Illuminate\Http\Request;
use App\Http\Factories\UserFactory;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Owner\ProfileFormRequest;
use App\Http\Requests\Owner\OwnerStore;
use Validator;

class OwnerController extends ApiController
{
    public function show(string $user): JsonResponse
    {
        try {
            $data = (new UserFactory())->getUser($user, 'owners/%d');
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }

    public function store(Request $request): JsonResponse
    {
        $rules = (new OwnerStore())->rules();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        try {
            $data = (new UserFactory())->storeUser($request, 'owners/store');
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }

    public function completeProfile(Request $request, $id): JsonResponse
    {
        $owner_id = $id;

        try {
            $data = (new UserFactory())->updateOwner($request, $owner_id, 'owners/%d/profile-complete');
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }

    public function updateProfile(Request $request, $id): JsonResponse
    {
        $rules = (new ProfileFormRequest())->rules();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $owner_id = $id;

        try {
            $data = (new UserFactory())->updateOwner($request, $owner_id, 'owners/%d/profile-update');
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }
}
