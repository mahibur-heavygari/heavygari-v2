<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Http\Factories\UserFactory;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Owner\CompleteProfileFormRequest;
use App\Http\Requests\Owner\ProfileFormRequest;
use Validator;

class ProfileController extends ApiController
{
    public function show(): JsonResponse
    {
        $owner_id = Auth::user()->profileOwner->id;

        try {
            $data = (new UserFactory())->getUser($owner_id, 'owners/%d');
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }

    public function completeProfile(Request $request): JsonResponse
    {
        $rules = (new CompleteProfileFormRequest())->rules();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $owner_id = Auth::user()->profileOwner->id;

        try {
            $data = (new UserFactory())->updateOwner($request, $owner_id, 'owners/%d/profile-complete');
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }

    public function updateProfile(Request $request): JsonResponse
    {
        $rules = (new ProfileFormRequest())->rules();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $owner_id = Auth::user()->profileOwner->id;

        try {
            $data = (new UserFactory())->updateOwner($request, $owner_id, 'owners/%d/profile-update');
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }
}
