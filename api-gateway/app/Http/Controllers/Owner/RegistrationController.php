<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Http\Factories\UserFactory;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Requests\Owner\RegistrationFormRequest;
use App\Libraries\UserLib\Login\LoginManager;
use App\User;
use Validator;
use App\Http\Controllers\ApiController;
use Carbon;

class RegistrationController extends ApiController
{
    public function checkPhone(Request $request)
    {
        $rules =  [
            'phone' => 'required|numeric|digits:11|regex:/(01)[0-9]/'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError([['field'=>'phone', 'errors'=>['You must enter a valid phone number']]]);
        }

        try {
            $data = (new UserFactory())->checkPhone( $request, '/owner/check-phone' );
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }

    public function register(Request $request)
    {
        $rules = (new RegistrationFormRequest())->rules();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        try {
            $data = (new UserFactory())->register( $request, '/owner/register' );
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }

    public function matchPin ( Request $request )
    {
        $rules =  [
            'phone' => 'required',
            'pin' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        try {
            $data = (new UserFactory())->matchPin( $request, '/owner/match-pin' );
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }

    public function matchPassword ( Request $request )
    {
        $rules =  [
            'phone' => 'required',
            'password' => 'required',
            'user_panel' => 'required',
            'platform' => 'required',
            'os' => 'required',
            'fcm_token' => 'string'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        try {
            $data = (new UserFactory())->matchPassword( $request, '/owner/match-password' );
            
            $user_panel = $request->user_panel;
            $platform = $request->platform;
            $os = $request->os;
            $fcm_token = $request->fcm_token;
            $request['username'] = $request->phone;

            $login_manager = new LoginManager();
            $res = $login_manager->complete( $request, $user_panel, $platform, $os, $fcm_token );
            $content['token'] = $res['content']['token'];

            return response()->json(['data' => $content], $res['content']['status']);

        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return response()->json(['data' => $content], $res['content']['status']);
    }
}