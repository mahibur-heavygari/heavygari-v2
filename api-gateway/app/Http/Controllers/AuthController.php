<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Libraries\UserLib\Login\LoginManager;
use App\Http\Controllers\ApiController;
use DB;
use Validator;

class AuthController extends ApiController
{
    public function login(Request $request)
    {
        $rules =  [
            'username' => 'required',
            'password' => 'required',
            'user_panel' => 'required',
            'platform' => 'required',
            'os' => 'required',
            'fcm_token' => 'string'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

    	$username = $request->username;
    	$password = $request->password;
        $user_panel = $request->user_panel;

    	$login_manager = new LoginManager();
        $login_user = $login_manager->login( $username, $password, $user_panel );

        if( !$login_user ){
        	return $this->respondErrorInDetails('Sorry! could not login.', $login_manager->getError());
        }

        $panels = $login_manager->getPanels();

    	$platform = $request->platform;
    	$os = $request->os;
    	$fcm_token = $request->fcm_token;

    	$res = $login_manager->complete( $request, $user_panel, $platform, $os, $fcm_token );

        $this->content['token'] = $res['content']['token'];
        $this->content['panels'] = $panels;

    	return response()->json(['data' => $this->content], $res['content']['status']);
    }

    public function logout(Request $request)
    {    
        Auth::user()->token()->revoke();
        return response()->json( ['data' => [
            'message' => 'Successfully logged out'
        ]]);
    }
}
