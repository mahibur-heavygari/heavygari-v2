<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Factories\FullBookingFactory;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Customer\FullBookingCreate;
use Validator;

class FullBookingController extends ApiController
{
    public function getVehicleTypes(): JsonResponse
    {
        try {
            $data = (new FullBookingFactory())->vehicleTypes('vehicle-types' );
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }

    public function getCost( Request $request ): JsonResponse
    {
        try {
            $data = (new FullBookingFactory())->getCost( $request, 'get-cost' );
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }

    public function create( Request $request ): JsonResponse
    {
        $rules = (new FullBookingCreate())->rules();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $customer_id = Auth::user()->profileCustomer->id;

        try {
            $data = (new FullBookingFactory())->create( $request, $customer_id, 'customers/%d/create' );
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }
}
