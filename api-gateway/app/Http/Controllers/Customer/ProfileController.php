<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Factories\UserFactory;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Customer\ProfileFormRequest;
use Validator;

class ProfileController extends ApiController
{
    public function show(): JsonResponse
    {
        $customer_id = Auth::user()->profileCustomer->id;

        try {
            $data = (new UserFactory())->getUser($customer_id, 'customers/%d');
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }

    public function updateProfile(Request $request): JsonResponse
    {
        $rules = (new ProfileFormRequest())->rules();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $customer_id = Auth::user()->profileCustomer->id;

        try {
            $data = (new UserFactory())->updateCustomer($request, $customer_id, 'customers/%d/profile-update');
        }catch (HttpException $exception) {
            return new JsonResponse(
                json_decode($exception->getMessage()),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse($data, 200);
    }
}
