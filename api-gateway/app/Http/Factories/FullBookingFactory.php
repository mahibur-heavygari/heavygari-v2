<?php

namespace App\Http\Factories;

use Illuminate\Http\Request;
use App\Http\Clients\FullBookingHttpClient;
use Illuminate\Support\Collection;
use Symfony\Component\HttpKernel\Exception\HttpException;


class FullBookingFactory
{
    private $fullBookingClient;

    public function __construct()
    {
        $this->fullBookingClient = new FullBookingHttpClient();
    }

    public function vehicleTypes($tail_url): Collection
    {
        try {
            $res["data"] = $this->fullBookingClient->vehicleTypes($tail_url);
        }catch (HttpException $exception) {
            throw new HttpException($exception->getStatusCode(), $exception->getMessage());
        }

        return new Collection($res);
    }

    public function getCost(Request $request, $tail_url): Collection
    {
        try {
            $res["data"] = $this->fullBookingClient->getCost($request, $tail_url);
        }catch (HttpException $exception) {
            throw new HttpException($exception->getStatusCode(), $exception->getMessage());
        }

        return new Collection($res);
    }

    public function create(Request $request, $customer_id, $tail_url): Collection
    {
        try {
            $res["data"] = $this->fullBookingClient->create($request, $customer_id, $tail_url);
        }catch (HttpException $exception) {
            throw new HttpException($exception->getStatusCode(), $exception->getMessage());
        }

        return new Collection($res);
    }
}