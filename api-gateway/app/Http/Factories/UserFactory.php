<?php

namespace App\Http\Factories;

use Illuminate\Http\Request;
use App\Http\Clients\UserHttpClient;
use Illuminate\Support\Collection;
use Symfony\Component\HttpKernel\Exception\HttpException;


class UserFactory
{
    private $userClient;

    public function __construct()
    {
        $this->userClient = new UserHttpClient();
    }

    public function getUser(string $userId, $tail_url): Collection
    {
        try {
            $res["data"] = $this->userClient->getUser($userId, $tail_url);
        }catch (HttpException $exception) {
            throw new HttpException($exception->getStatusCode(), $exception->getMessage());
        }

        return new Collection($res);
    }

    public function storeUser(Request $request, $tail_url): Collection
    {
        try {
            $res["data"] = $this->userClient->storeUser($request, $tail_url);
        }catch (HttpException $exception) {
            throw new HttpException($exception->getStatusCode(), $exception->getMessage());
        }

        return new Collection($res);
    }

    public function checkPhone(Request $request, $tail_url): Collection
    {
        try {
            $res["data"] = $this->userClient->checkPhone($request, $tail_url);
        }catch (HttpException $exception) {
            throw new HttpException($exception->getStatusCode(), $exception->getMessage());
        }

        return new Collection($res);
    }

    public function register(Request $request, $tail_url): Collection
    {
        try {
            $res["data"] = $this->userClient->register($request, $tail_url);
        }catch (HttpException $exception) {
            throw new HttpException($exception->getStatusCode(), $exception->getMessage());
        }

        return new Collection($res);
    }

    public function matchPin(Request $request, $tail_url): Collection
    {
        try {
            $res["data"] = $this->userClient->matchPin($request, $tail_url);
        }catch (HttpException $exception) {
            throw new HttpException($exception->getStatusCode(), $exception->getMessage());
        }

        return new Collection($res);
    }

    public function matchPassword(Request $request, $tail_url): Collection
    {
        try {
            $res["data"] = $this->userClient->matchPassword($request, $tail_url);
        }catch (HttpException $exception) {
            throw new HttpException($exception->getStatusCode(), $exception->getMessage());
        }

        return new Collection($res);
    }

    public function sendPinAtForgetPassword(Request $request, $tail_url): Collection
    {
        try {
            $res["data"] = $this->userClient->sendPinAtForgetPassword($request, $tail_url);
        }catch (HttpException $exception) {
            throw new HttpException($exception->getStatusCode(), $exception->getMessage());
        }

        return new Collection($res);
    }

    public function matchPinAtForgetPassword(Request $request, $tail_url): Collection
    {
        try {
            $res["data"] = $this->userClient->matchPinAtForgetPassword($request, $tail_url);
        }catch (HttpException $exception) {
            throw new HttpException($exception->getStatusCode(), $exception->getMessage());
        }

        return new Collection($res);
    }

    public function updateOwner(Request $request, $id, $tail_url): Collection
    {
        try {

            $this->userClient->uploadFile($request, $id, 'owners/%d/upload-photo', 'photo');
            $this->userClient->uploadFile($request, $id, 'owners/%d/upload-ownership-card-photo', 'ownership_card_photo');
            $this->userClient->uploadFile($request, $id, 'owners/%d/upload-nid-photo', 'nid_photo');

            $res["data"] = $this->userClient->update($request, $id, $tail_url);
            
        }catch (HttpException $exception) {
            throw new HttpException($exception->getStatusCode(), $exception->getMessage());
        }

        return new Collection($res);
    }

    public function updateCustomer(Request $request, $id, $tail_url): Collection
    {
        try {
            $this->userClient->uploadFile($request, $id, 'customers/%d/upload-photo', 'photo');

            $res["data"] = $this->userClient->update($request, $id, $tail_url);
            
        }catch (HttpException $exception) {
            throw new HttpException($exception->getStatusCode(), $exception->getMessage());
        }

        return new Collection($res);
    }
}