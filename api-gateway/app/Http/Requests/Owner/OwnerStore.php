<?php

namespace App\Http\Requests\Owner;

class OwnerStore
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'role' => 'required',
            'password' => 'required|min:6',
            'company_name' => '',
            'photo' => 'file|max:600',
            'thumb_photo' => '',
            'ownership_card_number' => '',
            'ownership_card_photo' => 'file',
            'nid_number' => 'required',
            'nid_photo' => 'file',
            'address' => 'required',
            'about' => '',
            'bank_name' => '',
            'bank_branch' => '',
            'account_number' => ''
        ];
    }
}
