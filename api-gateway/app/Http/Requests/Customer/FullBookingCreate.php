<?php

namespace App\Http\Requests\Customer;

class FullBookingCreate
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehicle_type' => 'required',
            'trip_type' => 'required',
            'from' => 'required',
            'to' => 'required',
            'recipient_name' => 'required',
            'recipient_phone' => 'required',
            'payment_by' => 'required',
            'particular_details' => 'required',
            'waiting_time' => '',
            'special_instruction' => ''
        ];
    }
}
