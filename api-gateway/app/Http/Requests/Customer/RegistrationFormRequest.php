<?php

namespace App\Http\Requests\Customer;

//use Illuminate\Foundation\Http\FormRequest;

class RegistrationFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'phone' => 'required',
            'name' => 'required',
            'password' => 'required|min:6',
            'platform' => 'required',
            'os' => 'required',
            'fcm_token' => 'string'
        ];
    }
}
