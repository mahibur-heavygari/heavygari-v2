<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Enumarations\ApiErrorCodes;

class Permission
{
    /**
     * Authentication (role based) for api calls
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role = null)
    {
        $permission = Auth::user()->permissions->where('code', $role)->first();

        if ( is_null($permission) ) {

            return response()->json(['error' => [
                'message' => 'Access denied. You may not have the appropriate permissions to access.',
                'type' => 'access_denied',
                'error_details' => [
                    'error_code' => ApiErrorCodes::$ACCESS_DENIED,
                    'error_title' => 'Access denied',
                    'error_feild' => NULL
                ],
            ]], 401);
        }

        return $next($request);
    }
}
