<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use ProfileOwner;
use App\Enumarations\ApiErrorCodes;

class CompleteProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role = null)
    {
        $user = Auth::user();

        if( $user->status == 'inactive' || $user->status == 'blocked' || $user->status == 'deleted' ){

            return response()->json(['error' => [
                'message' => 'Unauthorized user request. The user is either inactive or blocked',
                'type' => 'inactive_blocked_deleted_user',
                'error_details' => [
                    'error_code' => ApiErrorCodes::$INACTIVE_BLOCKED_DELETED_USER,
                    'error_title' => 'Unauthorized user request',
                    'error_feild' => NULL
                ],
            ]], 401);
        }

        if( $role == 'owner' && $user->profileOwner->profile_complete == 'no'){
            
            return response()->json(['error' => [
                'message' => 'Unauthorized profile request. Profile is not completed',
                'type' => 'incomplete_profile',
                'error_details' => [
                    'error_code' => ApiErrorCodes::$INCOMOPLETE_PROFILE,
                    'error_title' => 'Unauthorized profile request',
                    'error_feild' => NULL
                ],
            ]], 401);
        }

        return $next($request);
    }
}
