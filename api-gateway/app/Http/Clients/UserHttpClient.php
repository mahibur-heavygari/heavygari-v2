<?php

namespace App\Http\Clients;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class UserHttpClient
 * @package App\Http\Clients
 * @author Chrysovalantis Koutsoumpos <chrysovalantis.koutsoumpos@devmob.com>
 */
class UserHttpClient
{
    /**
     * @var Client
     */
    private $client;

    /**
     * UserHttpClient constructor.
     */
    public function __construct()
    {
        $this->client = new Client(['base_uri' => config('heavygari.user_service_host')]);
    }

    /**
     * @param string $user
     * @return string
     */
    public function getUser(string $user, $tail_url)
    {
        try {
            $response = $this->client->get(sprintf($tail_url, $user));
        } catch (BadResponseException $exception) {
            throw new HttpException(
                $exception->getCode(),
                $exception->getResponse()->getBody()->getContents()
            );
        }

        return json_decode($response->getBody()->getContents());
    }

    public function storeUser(Request $request, $tail_url)
    {
        try {
            $response = $this->client->post( $tail_url,
                array( 'form_params' => $request->all() )
            );
        } catch (BadResponseException $exception) {
            throw new HttpException(
                $exception->getCode(),
                $exception->getResponse()->getBody()->getContents()
            );
        }

        return json_decode($response->getBody()->getContents());
    }

    public function checkPhone(Request $request, $tail_url)
    {
        $parameters['phone'] = $request->phone;

        try {
            $response = $this->client->post( $tail_url,
                array( 'form_params' => $parameters )
            );
        } catch (BadResponseException $exception) {
            throw new HttpException(
                $exception->getCode(),
                $exception->getResponse()->getBody()->getContents()
            );
        }

        return json_decode($response->getBody()->getContents());
    }

    public function register(Request $request, $tail_url)
    {
        $parameters['phone'] = $request->phone;
        $parameters['name'] = $request->name;
        $parameters['email'] = $request->email;
        $parameters['password'] = $request->password;

        try {
            $response = $this->client->post( $tail_url,
                array( 'form_params' => $parameters )
            );
        } catch (BadResponseException $exception) {
            throw new HttpException(
                $exception->getCode(),
                $exception->getResponse()->getBody()->getContents()
            );
        }

        return json_decode($response->getBody()->getContents());
    }

    public function matchPin(Request $request, $tail_url)
    {
        $parameters['phone'] = $request->phone;
        $parameters['pin'] = $request->pin;

        try {
            $response = $this->client->post( $tail_url,
                array( 'form_params' => $parameters )
            );
        } catch (BadResponseException $exception) {
            throw new HttpException(
                $exception->getCode(),
                $exception->getResponse()->getBody()->getContents()
            );
        }

        return json_decode($response->getBody()->getContents());
    }

    public function matchPassword(Request $request, $tail_url)
    {
        $parameters['phone'] = $request->phone;
        $parameters['password'] = $request->password;

        try {
            $response = $this->client->post( $tail_url,
                array( 'form_params' => $parameters )
            );
        } catch (BadResponseException $exception) {
            throw new HttpException(
                $exception->getCode(),
                $exception->getResponse()->getBody()->getContents()
            );
        }

        return json_decode($response->getBody()->getContents());
    }

    public function sendPinAtForgetPassword(Request $request, $tail_url)
    {
        $parameters['phone'] = $request->phone;

        try {
            $response = $this->client->post( $tail_url,
                array( 'form_params' => $parameters )
            );
        } catch (BadResponseException $exception) {
            throw new HttpException(
                $exception->getCode(),
                $exception->getResponse()->getBody()->getContents()
            );
        }

        return json_decode($response->getBody()->getContents());
    }

    public function matchPinAtForgetPassword(Request $request, $tail_url)
    {
        $parameters['phone'] = $request->phone;
        $parameters['pin'] = $request->pin;

        try {
            $response = $this->client->post( $tail_url,
                array( 'form_params' => $parameters )
            );
        } catch (BadResponseException $exception) {
            throw new HttpException(
                $exception->getCode(),
                $exception->getResponse()->getBody()->getContents()
            );
        }

        return json_decode($response->getBody()->getContents());
    }

    public function update(Request $request, $id, $tail_url)
    {
        try {
            $response = $this->client->post( sprintf($tail_url, $id),
                array( 
                    'form_params' => $request->all()
                )
            );

        } catch (BadResponseException $exception) {
            throw new HttpException(
                $exception->getCode(),
                $exception->getResponse()->getBody()->getContents()
            );
        }
        return json_decode($response->getBody()->getContents());
    }

    public function uploadFile(Request $request, $id, $tail_url, $name)
    {
        try {
            if ($request->hasFile($name)){
                $this->client->post( sprintf($tail_url, $id),
                    array(
                        'multipart' =>
                        [
                            [
                                'name'     => 'file',
                                'contents' => file_get_contents($request->file($name)->path()),
                                'filename' => $request->file($name)->getClientOriginalName()
                            ],
                        ]
                    )
                );
            }
        } catch (BadResponseException $exception) {
            throw new HttpException(
                $exception->getCode(),
                $exception->getResponse()->getBody()->getContents()
            );
        }
    }
}
