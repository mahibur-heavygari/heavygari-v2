<?php

namespace App\Http\Clients;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class FullBookingHttpClient
{
    private $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => config('heavygari.full_booking_service_host')]);
    }

    public function vehicleTypes($tail_url)
    {
        try {
            $response = $this->client->get($tail_url);

        } catch (BadResponseException $exception) {
            throw new HttpException(
                $exception->getCode(),
                $exception->getResponse()->getBody()->getContents()
            );
        }

        return json_decode($response->getBody()->getContents());
    }

    public function getCost(Request $request, $tail_url)
    {
        $parameters['vehicle_type'] = $request->vehicle_type;
        $parameters['trip_type'] = $request->trip_type;
        $parameters['from'] = $request->from;
        $parameters['to'] = $request->to;

        try {
            $response = $this->client->post( $tail_url,
                array( 'form_params' => $parameters )
            );
        } catch (BadResponseException $exception) {
            throw new HttpException(
                $exception->getCode(),
                $exception->getResponse()->getBody()->getContents()
            );
        }
        return json_decode($response->getBody()->getContents());
    }

    public function create(Request $request, $customer_id, $tail_url)
    {
        $parameters['vehicle_type'] = $request->vehicle_type;
        $parameters['trip_type'] = $request->trip_type;
        $parameters['from'] = $request->from;
        $parameters['to'] = $request->to;
        $parameters['recipient_name'] = $request->recipient_name;
        $parameters['recipient_phone'] = $request->recipient_phone;
        $parameters['payment_by'] = $request->payment_by;
        $parameters['particular_details'] = $request->particular_details;
        $parameters['waiting_time'] = $request->waiting_time;
        $parameters['special_instruction'] = $request->special_instruction;
        $parameters['panel'] = $request->panel;

        try {
            $response = $this->client->post( sprintf($tail_url, $customer_id),
                array( 'form_params' => $parameters )
            );
        } catch (BadResponseException $exception) {
            throw new HttpException(
                $exception->getCode(),
                $exception->getResponse()->getBody()->getContents()
            );
        }
        return json_decode($response->getBody()->getContents());
    }
}
