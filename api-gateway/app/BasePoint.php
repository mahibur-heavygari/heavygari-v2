<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BasePoint extends Model
{    
    protected $fillable = ['title', 'fullname', 'lat', 'lon'];
}
