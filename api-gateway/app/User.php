<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'email', 'password', 'phone', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeBlocked($query)
    {
        return $query->where('status', 'blocked');
    }

    public function roles() {
        return $this->belongsToMany(Role::class);
    }

    public function permissions() {
        return $this->belongsToMany(Permission::class);
    }

    public function profileAdmin()
    {
        return $this->hasOne('App\ProfileAdmin');
    }

    public function profileCustomer()
    {
        return $this->hasOne('App\ProfileCustomer');
    }

    public function profileOwner()
    {
        return $this->hasOne('App\ProfileOwner');
    }

    public function profileDriver()
    {
        return $this->hasOne('App\ProfileDriver');
    }

    public function profileVehicle()
    {
        return $this->hasOne('App\ProfileVehicle');
    }

}
