<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
    protected $table = 'base_vehicle_types';

    protected $fillable = [
        'title', 'title_bn', 'capacity_type_id', 'capacity', 'max_capacity', 'icon_url'
    ];    
}
