<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleCapacityType extends Model
{
    protected $table = 'base_vehicle_capacity_types';

    protected $fillable = [
        'title'
    ];

}
