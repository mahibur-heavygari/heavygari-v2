<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFullTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('full_trips', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            $table->bigInteger('booking_id')->unsigned();
            $table->integer('from_point')->unsigned()->nullable();
            $table->text('from_address');
            $table->decimal('from_lat', 10, 8);
            $table->decimal('from_lon', 11, 8);
            $table->integer('to_point')->unsigned()->nullable();
            $table->text('to_address');            
            $table->decimal('to_lat', 10, 8);
            $table->decimal('to_lon', 11, 8);
            $table->float('distance');
            $table->string('duration');
            $table->integer('duration_in_second');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('booking_id')->references('id')->on('full_bookings')->onDelete('cascade');
            $table->foreign('from_point')->references('id')->on('base_points')->onDelete('cascade');
            $table->foreign('to_point')->references('id')->on('base_points')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('full_trips');
    }
}
