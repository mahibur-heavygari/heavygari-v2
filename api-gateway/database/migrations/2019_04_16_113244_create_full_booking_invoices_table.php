<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFullBookingInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('full_booking_invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('booking_id')->unsigned();
            $table->decimal('pickup_charge', 10, 2)->nullable();
            $table->decimal('dropoff_charge', 10, 2)->nullable();           
            $table->decimal('base_fare', 10, 2)->nullable();
            $table->decimal('distance_cost', 10, 2);
            $table->decimal('weight_cost', 10, 2)->nullable();
            $table->decimal('surcharge', 10, 2)->nullable();
            $table->decimal('waiting_cost', 10, 2)->nullable();
            $table->decimal('triptime_cost', 10, 2)->nullable();
            $table->decimal('heavygari_fee', 10, 2)->nullable();
            $table->decimal('total_fare', 10, 2);
            $table->decimal('discount', 10, 2)->nullable();
            $table->decimal('total_cost', 10, 2);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('booking_id')->references('id')->on('full_bookings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('full_booking_invoices');
    }
}
