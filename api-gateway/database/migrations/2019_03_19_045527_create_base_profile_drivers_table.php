<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseProfileDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_profile_drivers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->string('reference_number')->nullable();
            $table->enum('profile_complete', ['no', 'yes'])->default('no');
            $table->string('license_number')->nullable();
            $table->string('license_photo')->nullable();
            $table->string('license_date_of_issue')->nullable();
            $table->string('license_date_of_expire')->nullable();
            $table->string('license_issuing_authority')->nullable();   
            $table->string('photo')->nullable();
            $table->string('thumb_photo')->nullable();  
            $table->string('nid_number')->nullable();
            $table->string('nid_photo')->nullable();  
            $table->text('about')->nullable();
            $table->timestamp('last_login')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_profile_drivers');
    }
}
