<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFullBookingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('full_booking_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('booking_id')->unsigned();
            $table->string('capacity')->nullable();
            $table->enum('trip_type', ['single', 'round'])->default('single');
            $table->string('invoice_pdf')->nullable();
            $table->integer('product_category_id')->unsigned()->nullable();
            $table->mediumText('product_category_text')->nullable();
            $table->string('waiting_time')->nullable();
            $table->string('recipient_name');
            $table->string('recipient_phone');
            $table->enum('payment_by', ['customer', 'recipient']);
            $table->enum('is_payment_collected', ['no', 'yes'])->default('no');
            $table->text('special_instruction')->nullable();
            $table->decimal('multiplier', 10, 2)->nullable();
            $table->enum('distance_type', ['short', 'normal', 'long']);
            $table->float('total_distance');
            $table->integer('total_duration');

            $table->string('created_by')->nullable();
            $table->timestamp('accepted_at')->nullable();
            $table->string('accepted_by')->nullable();
            $table->timestamp('started_at')->nullable();
            $table->string('started_by')->nullable();
            $table->timestamp('completed_at')->nullable();
            $table->string('completed_by')->nullable();
            $table->timestamp('cancelled_at')->nullable();
            $table->string('cancelled_by')->nullable();
            $table->string('cancelled_reason')->nullable();

            $table->bigInteger('accept_time_profile_driver_id')->unsigned()->nullable(); 
            $table->bigInteger('start_time_profile_driver_id')->unsigned()->nullable(); 
            $table->bigInteger('complete_time_profile_driver_id')->unsigned()->nullable(); 
            $table->bigInteger('cancel_time_profile_driver_id')->unsigned()->nullable(); 

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('booking_id')->references('id')->on('full_bookings')->onDelete('cascade');
            $table->foreign('product_category_id')->references('id')->on('base_product_categories')->onDelete('cascade');
            $table->foreign('accept_time_profile_driver_id')->references('id')->on('base_profile_drivers')->onDelete('cascade');
            $table->foreign('start_time_profile_driver_id')->references('id')->on('base_profile_drivers')->onDelete('cascade');
            $table->foreign('complete_time_profile_driver_id')->references('id')->on('base_profile_drivers')->onDelete('cascade');
            $table->foreign('cancel_time_profile_driver_id')->references('id')->on('base_profile_drivers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('full_booking_details');
    }
}
