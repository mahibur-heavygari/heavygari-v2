<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFullBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('full_bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unique_id')->unique();     
            $table->bigInteger('profile_customer_id')->unsigned();   
            $table->bigInteger('profile_owner_id')->unsigned()->nullable();  
            $table->bigInteger('current_profile_driver_id')->unsigned()->nullable();  
            $table->bigInteger('profile_vehicle_id')->unsigned()->nullable();    
            $table->integer('vehicle_type_id')->unsigned();
            $table->timestamp('datetime')->nullable();
            $table->enum('booking_category', ['full', 'shared'])->default('full');
            $table->enum('booking_type', ['on-demand', 'advance'])->default('on-demand');
            $table->enum('status', ['open', 'accepted', 'arriving', 'ongoing', 'completed','cancelled'])->default('open');
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('profile_customer_id')->references('id')->on('base_profile_customers')->onDelete('cascade');
            $table->foreign('profile_owner_id')->references('id')->on('base_profile_owners')->onDelete('cascade');
            $table->foreign('current_profile_driver_id')->references('id')->on('base_profile_drivers')->onDelete('cascade');
            $table->foreign('profile_vehicle_id')->references('id')->on('base_profile_vehicles')->onDelete('cascade');
            $table->foreign('vehicle_type_id')->references('id')->on('base_vehicle_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('full_bookings');
    }
}
