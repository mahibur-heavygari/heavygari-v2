<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseProfileVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_profile_vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference_number')->nullable();
            $table->bigInteger('profile_owner_id')->unsigned();
            $table->bigInteger('current_driver_profile_id')->unsigned()->nullable();
            $table->string('name');
            $table->text('about')->nullable();
            $table->string('photo')->nullable();
            $table->string('thumb_photo')->nullable();
            $table->integer('vehicle_type_id')->unsigned();
            $table->string('vehicle_registration_number')->nullable();
            $table->string('fitness_number')->nullable();
            $table->string('fitness_expiry')->nullable();
            $table->string('number_plate')->nullable();
            $table->string('tax_token_number')->nullable();
            $table->string('tax_token_expirey')->nullable();
            $table->string('chesis_number')->nullable();
            $table->string('engine_capacity_cc')->nullable();
            $table->string('ride_sharing_certificate_number')->nullable();
            $table->string('insurance_number')->nullable();
            $table->timestamp('last_login')->nullable();
            $table->enum('status', ['available', 'booked', 'on-trip', 'off-duty'])->default('off-duty');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('profile_owner_id')->references('id')->on('base_profile_owners')->onDelete('cascade');
            $table->foreign('current_driver_profile_id')->references('id')->on('base_profile_drivers')->onDelete('set null');
            $table->foreign('vehicle_type_id')->references('id')->on('base_vehicle_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_profile_vehicles');
    }
}
