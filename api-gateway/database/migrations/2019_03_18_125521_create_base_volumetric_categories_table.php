<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseVolumetricCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_volumetric_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->unique();
            $table->integer('height')->comment('inch');
            $table->integer('width')->comment('inch');
            $table->integer('length')->comment('inch');
            $table->decimal('volumetric_weight', 10, 2)->comment('kg');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_volumetric_categories');
    }
}
