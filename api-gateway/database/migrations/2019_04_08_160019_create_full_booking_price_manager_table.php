<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFullBookingPriceManagerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('full_booking_price_manager', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('vehicle_type_id')->unsigned();
            $table->decimal('base_fare', 6, 2)->comment('taka');
            $table->decimal('short_trip_rate', 6, 2)->comment('/km');
            $table->decimal('up_trip_rate', 6, 2)->comment('/km');
            $table->decimal('down_trip_rate', 6, 2)->comment('/km');
            $table->decimal('long_up_trip_rate', 6, 2)->comment('/km');
            $table->decimal('long_down_trip_rate', 6, 2)->comment('/km');               
            $table->decimal('surcharge_rate', 6, 2)->comment('/km');
            $table->decimal('discount_percent', 6, 2)->comment('%');
            $table->decimal('admin_commission', 6, 2)->comment('%');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('vehicle_type_id')->references('id')->on('base_vehicle_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('full_booking_price_manager');
    }
}
