<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseProfileCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_profile_customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->enum('corporate', ['no', 'yes'])->default('no');   
            $table->enum('profile_complete', ['no', 'yes'])->default('no');      
            $table->string('photo')->nullable();
            $table->string('thumb_photo')->nullable();  
            $table->string('nid_number')->nullable();
            $table->string('nid_photo')->nullable();     
            $table->string('address')->nullable();
            $table->text('about')->nullable(); 
            $table->timestamp('last_login')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_profile_customers');
    }
}
