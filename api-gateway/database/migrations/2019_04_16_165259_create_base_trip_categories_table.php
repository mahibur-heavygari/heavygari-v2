<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseTripCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_trip_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('src_point_id')->unsigned();
            $table->integer('dest_point_id')->unsigned();
            $table->enum('trip_category', ['up', 'down'])->default('up');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['src_point_id', 'dest_point_id']);
            $table->foreign('src_point_id')->references('id')->on('base_points')->onDelete('cascade');
            $table->foreign('dest_point_id')->references('id')->on('base_points')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_trip_categories');
    }
}
