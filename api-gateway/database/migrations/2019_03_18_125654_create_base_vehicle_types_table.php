<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseVehicleTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_vehicle_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->unique();
            $table->string('title_bn')->unique();
            $table->float('capacity');
            $table->float('max_capacity');
            $table->integer('capacity_type_id')->unsigned();
            $table->string('icon_url')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('capacity_type_id')->references('id')->on('base_vehicle_capacity_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_vehicle_types');
    }
}
