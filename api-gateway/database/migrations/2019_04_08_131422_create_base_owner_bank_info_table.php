<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseOwnerBankInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_owner_bank_info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('profile_owner_id')->unsigned();
            $table->string('bank_name')->nullable();
            $table->string('bank_branch')->nullable();
            $table->string('account_number')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('profile_owner_id')->references('id')->on('base_profile_owners')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_owner_bank_info');
    }
}
