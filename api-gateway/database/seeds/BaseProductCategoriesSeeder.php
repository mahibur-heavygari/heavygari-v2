<?php

use Illuminate\Database\Seeder;
use App\BaseProductCategory;

class BaseProductCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertProductCategories();
        $this->command->info('Base product categories seeded!');
    }

    protected function insertProductCategories()
    {
        $product_categories = [
            [
                'title' => 'Electronics',
            ],
            [
                'title' => 'Furnitures',
            ],
			    [
                'title' => 'raw material',
            ],
			[
                'title' => 'spare parts',
            ],
            [
                'title' => 'Vehicles',
            ],
			    [
                'title' => 'Commodity',
            ],
			[
                'title' => 'Liquids',
            ],
            [
                'title' => 'Chemicals',
            ],
			    [
                'title' => 'Packaging material',
            ],
			[
                'title' => 'Document',
            ],
            [
                'title' => 'Garments',
            ],
			    [
                'title' => 'Fragile Item',
            ],
			[
                'title' => 'Food',
            ],
            [
                'title' => 'Cosmetics',
            ],
			    [
                'title' => 'Leather Goods',
            ],
			[
                'title' => 'Accessories',
            ],
            [
                'title' => 'Hardware',
            ]		
        ];

        foreach ($product_categories as $key => $category) {
            BaseProductCategory::updateOrCreate( $category );
        }
    }
}
