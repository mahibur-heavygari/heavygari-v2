<?php

use Illuminate\Database\Seeder;
use App\FullBookingPriceManager;

class BasePriceManagerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertFullBookingPriceManagerInfo();
        $this->command->info('Price manager seeded!');
    }
    
    protected function insertFullBookingPriceManagerInfo() 
    {
        $prices = [
            [
                'vehicle_type_id' => 1,
                'base_fare' => 2000.00,
                'short_trip_rate' => 450.00,
                'up_trip_rate' => 38.00,
                'down_trip_rate' => 34.00,
                'long_up_trip_rate' => 48.00,
                'long_down_trip_rate' => 44.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 2,
                'base_fare' => 2500.00,
                'short_trip_rate' => 500.00,
                'up_trip_rate' => 38.00,
                'down_trip_rate' => 34.00,
                'long_up_trip_rate' => 50.00,
                'long_down_trip_rate' => 46.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 3,
                'base_fare' => 1000.00,
                'short_trip_rate' => 450.00,
                'up_trip_rate' => 48.00,
                'down_trip_rate' => 44.00,
                'long_up_trip_rate' => 58.00,
                'long_down_trip_rate' => 54.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 4,
                'base_fare' => 700.00,
                'short_trip_rate' => 200.00,
                'up_trip_rate' => 33.00,
                'down_trip_rate' => 29.00,
                'long_up_trip_rate' => 43.00,
                'long_down_trip_rate' => 38.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 5,
                'base_fare' => 2500.00,
                'short_trip_rate' => 500.00,
                'up_trip_rate' => 38.00,
                'down_trip_rate' => 34.00,
                'long_up_trip_rate' => 48.00,
                'long_down_trip_rate' => 44.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 6,
                'base_fare' => 3000.00,
                'short_trip_rate' => 600.00,
                'up_trip_rate' => 80.00,
                'down_trip_rate' => 78.00,
                'long_up_trip_rate' => 90.00,
                'long_down_trip_rate' => 88.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],        
            [   
                'vehicle_type_id' => 7,
                'base_fare' => 2500.00,
                'short_trip_rate' => 500.00,
                'up_trip_rate' => 70.00,
                'down_trip_rate' => 68.00,
                'long_up_trip_rate' => 80.00,
                'long_down_trip_rate' => 78.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 8,
                'base_fare' => 1000.00,
                'short_trip_rate' => 150.00,
                'up_trip_rate' => 38.00,
                'down_trip_rate' => 34.00,
                'long_up_trip_rate' => 48.00,
                'long_down_trip_rate' => 44.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 9,
                'base_fare' => 1000.00,
                'short_trip_rate' => 300.00,
                'up_trip_rate' => 40.00,
                'down_trip_rate' => 36.00,
                'long_up_trip_rate' => 50.00,
                'long_down_trip_rate' => 46.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 10,
                'base_fare' => 3000.00,
                'short_trip_rate' => 2000.00,
                'up_trip_rate' => 2000.00,
                'down_trip_rate' => 1998.00,
                'long_up_trip_rate' => 2000.00,
                'long_down_trip_rate' => 1998.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 11,
                'base_fare' => 3000.00,
                'short_trip_rate' => 450.00,
                'up_trip_rate' => 48.00,
                'down_trip_rate' => 44.00,
                'long_up_trip_rate' => 48.00,
                'long_down_trip_rate' => 44.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 12,
                'base_fare' => 2000.00,
                'short_trip_rate' => 450.00,
                'up_trip_rate' => 40.00,
                'down_trip_rate' => 36.00,
                'long_up_trip_rate' => 48.00,
                'long_down_trip_rate' => 44.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 13,
                'base_fare' => 4000.00,
                'short_trip_rate' => 700.00,
                'up_trip_rate' => 100.00,
                'down_trip_rate' => 93.00,
                'long_up_trip_rate' => 120.00,
                'long_down_trip_rate' => 113.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 14,
                'base_fare' => 2500.00,
                'short_trip_rate' => 1000.00,
                'up_trip_rate' => 80.00,
                'down_trip_rate' => 76.00,
                'long_up_trip_rate' => 85.00,
                'long_down_trip_rate' => 80.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 15,
                'base_fare' => 4000.00,
                'short_trip_rate' => 1000.00,
                'up_trip_rate' => 82.00,
                'down_trip_rate' => 78.00,
                'long_up_trip_rate' => 92.00,
                'long_down_trip_rate' => 88.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 16,
                'base_fare' => 4000.00,
                'short_trip_rate' => 700.00,
                'up_trip_rate' => 40.00,
                'down_trip_rate' => 36.00,
                'long_up_trip_rate' => 48.00,
                'long_down_trip_rate' => 44.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 17,
                'base_fare' => 5000.00,
                'short_trip_rate' => 800.00,
                'up_trip_rate' => 50.00,
                'down_trip_rate' => 46.00,
                'long_up_trip_rate' => 55.00,
                'long_down_trip_rate' => 50.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
            [
                'vehicle_type_id' => 18,
                'base_fare' => 50.00,
                'short_trip_rate' => 24.00,
                'up_trip_rate' => 18.00,
                'down_trip_rate' => 14.00,
                'long_up_trip_rate' => 15.00,
                'long_down_trip_rate' => 13.00,
                'surcharge_rate' => 0.00,
                'discount_percent' => 0.00,
                'admin_commission' => 7.00
            ],
        ];

        foreach ($prices as $key => $price) {
            FullBookingPriceManager::updateOrCreate( $price );
        }
    }
}
 //Full booking price manager insertion

/*INSERT INTO `heavygari-v2`.full_booking_price_manager (`id`, `vehicle_type_id`, `base_fare`, `short_trip_rate`, `up_trip_rate`, `down_trip_rate`, `long_up_trip_rate`, `long_down_trip_rate`, `surcharge_rate`, `discount_percent`, `admin_commission`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '3000.00', '48.00', '47.00', '45.00', '45.00', '42.00', '0.00', '0.00', '10.00', NULL, '2019-04-15 17:56:08', NULL),
(2, 2, '3500.00', '54.00', '45.00', '40.00', '45.00', '42.00', '0.00', '0.00', '10.00', NULL, '2019-04-15 17:56:31', NULL),
(3, 3, '1500.00', '49.00', '47.00', '43.00', '57.00', '53.00', '0.00', '0.00', '10.00', NULL, '2019-04-15 17:56:53', NULL),
(4, 4, '1140.00', '42.00', '39.00', '35.00', '49.00', '44.00', '0.00', '0.00', '10.00', NULL, '2019-04-15 17:57:12', NULL),
(5, 5, '2500.00', '45.00', '35.00', '31.00', '45.00', '41.00', '0.00', '0.00', '10.00', NULL, '2018-06-06 09:01:48', NULL),
(6, 6, '5000.00', '85.00', '60.00', '55.00', '60.00', '55.00', '0.00', '40.00', '10.00', NULL, '2019-03-20 08:04:14', NULL),
(7, 7, '3500.00', '69.00', '59.00', '54.00', '69.00', '67.00', '0.00', '0.00', '10.00', NULL, '2018-06-06 09:04:19', NULL),
(8, 8, '1000.00', '47.00', '37.00', '33.00', '47.00', '43.00', '0.00', '0.00', '10.00', NULL, '2018-06-07 08:42:42', NULL),
(9, 9, '1000.00', '49.00', '39.00', '35.00', '49.00', '45.00', '0.00', '0.00', '10.00', NULL, '2018-06-07 08:42:56', NULL),
(10, 10, '4000.00', '50.00', '45.00', '42.00', '45.00', '40.00', '0.00', '0.00', '10.00', NULL, '2018-10-04 11:22:28', NULL),
(11, 11, '4000.00', '59.00', '53.00', '49.00', '57.00', '53.00', '0.00', '0.00', '10.00', NULL, '2018-06-07 08:43:16', NULL),
(12, 12, '3000.00', '57.00', '47.00', '43.00', '57.00', '53.00', '0.00', '0.00', '10.00', NULL, '2018-06-07 08:43:45', NULL),
(13, 13, '6000.00', '149.00', '99.00', '92.00', '119.00', '112.00', '0.00', '0.00', '10.00', NULL, '2018-06-07 08:44:01', NULL),
(14, 14, '3500.00', '89.00', '69.00', '67.00', '74.00', '69.00', '0.00', '0.00', '10.00', NULL, '2018-06-07 08:46:04', NULL),
(15, 15, '6000.00', '110.00', '110.00', '105.00', '110.00', '105.00', '0.00', '40.00', '10.00', NULL, '2019-03-20 08:05:03', NULL),
(16, 16, '5000.00', '80.00', '70.00', '60.00', '75.00', '60.00', '0.00', '0.00', '10.00', NULL, '2019-04-15 17:57:31', NULL),
(17, 17, '7000.00', '90.00', '80.00', '70.00', '80.00', '70.00', '0.00', '0.00', '10.00', NULL, '2019-04-15 17:57:52', NULL),
(18, 18, '49.00', '23.00', '17.00', '13.00', '14.00', '12.00', '0.00', '0.00', '10.00', NULL, '2018-06-06 08:59:18', NULL),
(19, 19, '1000.00', '35.00', '32.00', '30.00', '33.00', '30.00', '0.00', '0.00', '10.00', '2018-10-04 11:21:55', '2018-10-04 11:21:55', NULL),
(20, 20, '7000.00', '120.00', '120.00', '115.00', '120.00', '115.00', '0.00', '40.00', '10.00', '2018-10-25 08:41:02', '2019-03-20 08:05:43', NULL);*/
