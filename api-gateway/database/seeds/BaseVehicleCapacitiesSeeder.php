<?php

use Illuminate\Database\Seeder;
use App\VehicleCapacityType;

class BaseVehicleCapacitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertVehicleCapacityTypes();
        $this->command->info('Base Vehicle Capacity types seeded!');
    }

    protected function insertVehicleCapacityTypes()
    {
        $vehicle_capacity_types = [
            [
                'id' => 1,
                'title' => 'Kg',
            ],
            [
                'id' => 2,
                'title' => 'Ltr',
            ],
            [
                'id' => 3,
                'title' => 'Hours',
            ],
            [
                'id' => 4,
                'title' => 'Person',
            ],
            [
                'id' => 5,
                'title' => 'Size',
            ]
        ];

        foreach ($vehicle_capacity_types as $key => $category) {
            VehicleCapacityType::updateOrCreate( $category );
        }
    }
}
