<?php

use Illuminate\Database\Seeder;
use App\BaseVolumetricCategory;

class BaseVolumetricCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertVolumetricCategories();
        $this->command->info('Base volumetric categories seeded!');
    }

    protected function insertVolumetricCategories()
    {
        $volumetric_categories = [
            [
                'title' => 'Small',
                'height' => 5,
                'width' => 5,
                'length' => 6,
                'volumetric_weight' => .03
            ],
            [
                'title' => 'Medium',
                'height' => 10,
                'width' => 10,
                'length' => 12,
                'volumetric_weight' => .24
            ],
            [
                'title' => 'Large',
                'height' => 14,
                'width' => 14,
                'length' => 18,
                'volumetric_weight' => .71
            ],
            [
                'title' => 'Extra Large',
                'height' => 20,
                'width' => 20,
                'length' => 24,
                'volumetric_weight' => 1.92
            ],
            [
                'title' => 'Jumbo',
                'height' => 20,
                'width' => 20,
                'length' => 48,
                'volumetric_weight' => 3.84
            ]
        ];

        foreach ($volumetric_categories as $key => $category) {
            BaseVolumetricCategory::updateOrCreate( $category );
        }
    }
}
