<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertRoles();
        $this->command->info('Roles seeded!');
    }

    protected function insertRoles() {
        $admin_roles = [
            'admin_super' => 'Administrator',
            'admin_support' => 'Admin ( Support )',
            'admin_accountant' => 'Admin ( Accountant )',
            'admin_field_team' => 'Admin ( Field Team )'
        ];

        $customer_roles = [
            'customer_regular' => 'Customer',
        ];

        $owner_roles = [
            'owner_regular' => 'Owner',
        ];

        $driver_roles = [
            'driver_regular' => 'Driver'
        ];


        foreach ($admin_roles as $code => $name) {
            if(!Role::where('code', $code)->exists()) {
                Role::create([
                    "code" => $code,
                    "name" => $name,
                    "panel" => 'admin'
                ]);
            }
        }

        foreach ($customer_roles as $code => $name) {
            if(!Role::where('code', $code)->exists()) {
                Role::create([
                    "code" => $code,
                    "name" => $name,
                    "panel" => 'customer'
                ]);
            }
        }

        foreach ($owner_roles as $code => $name) {
            if(!Role::where('code', $code)->exists()) {
                Role::create([
                    "code" => $code,
                    "name" => $name,
                    "panel" => 'owner'
                ]);
            }
        }

        foreach ($driver_roles as $code => $name) {
            if(!Role::where('code', $code)->exists()) {
                Role::create([
                    "code" => $code,
                    "name" => $name,
                    "panel" => 'driver'
                ]);
            }
        }
    }
}
