<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BasePointsSeeder::class);

        $this->call(BaseProductCategoriesSeeder::class);

        $this->call(BaseWeightCategorySeeder::class);

        $this->call(BaseVolumetricCategorySeeder::class);

        $this->call(BaseVehicleCapacitiesSeeder::class);

        $this->call(BaseVehicleTypesSeeder::class);

        $this->call(BasePriceManagerSeeder::class);
        
        $this->call(RoleSeeder::class);
        
        $this->call(PermissionSeeder::class);
        
        $this->call(RoleWisePermissionSeeder::class);
        
        $this->call(AdminSeeder::class);
    }
}
