<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\ProfileAdmin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        $this->insertAdministrator();
        $this->command->info('Admin seeded!');
    }

    protected function insertAdministrator() {
        //Create Administrator
    	$email = 'admin@heavygari.com';
    	$role_code = 'admin_super';

    	$admin = User::where('username', $email);

    	if($admin->exists())
    		return;
    	
    	$usersArr = [
            'username' => $email,
            'password' => bcrypt('123456'),
            'name' => 'Admin',
            'email' => $email,
            'status' => 'active'
        ];
        $user = User::create($usersArr);

    	$profileArr = [
            'user_id' => $user->id,
            'profile_complete' => 'yes',
            'about' => 'Administrator of Heavygari',
        ];
        ProfileAdmin::create($profileArr);

        $role = Role::where('code', $role_code)->first();

        $user->roles()->attach($role);

        $permissions = $role->permissions;

        $user->permissions()->attach($permissions);
        

        //Create Support Admin
        $email = 'support@heavygari.com';
    	$role_code = 'admin_support';

    	$admin = User::where('username', $email);

    	if($admin->exists())
    		return;
    	
    	$usersArr = [
            'username' => $email,
            'password' => bcrypt('123456'),
            'name' => 'Support',
            'email' => $email,
            'status' => 'active'
        ];
        $user = User::create($usersArr);

    	$profileArr = [
            'user_id' => $user->id,
            'profile_complete' => 'yes',
            'about' => 'Support of Heavygari',
        ];
        ProfileAdmin::create($profileArr);

        $role = Role::where('code', $role_code)->first();

        $user->roles()->attach($role);

        $permissions = $role->permissions;

        $user->permissions()->attach($permissions);
    }
}
