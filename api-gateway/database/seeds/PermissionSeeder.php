<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertPermissions();
        $this->command->info('Permissions seeded!');
    }

    protected function insertPermissions() {
    	$permissions = [
    		'admin_user_management' => 'User Management',
    		'admin_booking_management' => 'Booking Management',
    		'admin_accounts_management' => 'Accounts Management',
    		'admin_analytics_management' => 'Analytics Management',
    		'admin_general' => 'General',

    		'owner_vehicle_management' => 'Vehicle Management',
    		'owner_driver_management' => 'Driver Management',
    		'owner_booking_management' => 'Booking Management',
    		'owner_billing_management' => 'Billing Management',
    		'owner_general' => 'General',

    		'customer_booking_management' => 'Booking Management',
    		'customer_billing_management' => 'Billing Management',
    		'customer_general' => 'General',
    		
    		'driver_general' => 'General',
    		
    		'vehicle_general' => 'General',
    	];

    	foreach ($permissions as $code => $name) {
    		if(!Permission::where('code', $code)->exists()) {
                Permission::create([
                    "code" => $code,
                    "name" => $name
                ]);
            }
    	}
    }
}
