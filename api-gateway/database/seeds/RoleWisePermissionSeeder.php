<?php

use Illuminate\Database\Seeder;
use App\Enumarations\RoleWisePermissions;
use App\Role;
use App\Permission;

class RoleWisePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        $this->insertRoleWisePermission();
        $this->command->info('Role wise permissions seeded!');
    }

    protected function insertRoleWisePermission() {
        $rolesWithPermissions = RoleWisePermissions::Role_WISE_PERMISSIONS;

        foreach ($rolesWithPermissions as $role_code => $permissions) {

        	foreach ($permissions as $permission_code => $permission_name) {

	        	$role = Role::where('code', $role_code)->first();
	        	$permission = Permission::where('code', $permission_code)->first();

	        	if ( !is_null($role) && !is_null($permission) ) {

	        		if(!$role->permissions()->wherePivot('permission_id', $permission->id)->exists())
	        			
						$role->permissions()->attach($permission);
	        	}

        	}
        }

    }
}
