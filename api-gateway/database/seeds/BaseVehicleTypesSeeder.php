<?php

use Illuminate\Database\Seeder;
use App\VehicleType;

class BaseVehicleTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertVehicleTypes();
        $this->command->info('Base Vehicle Types seeded!');
    }

    protected function insertVehicleTypes()
    {
        $vehicle_types = [
            [
                'id' => 1,
                'title' => 'Truck',
                'title_bn' => 'ট্রাক',
                'capacity' => 12000,
                'max_capacity' => 13000,
                'capacity_type_id' => 1,
                'icon_url' => 'public/vehicle_types/truck.png'
            ],
            [
                'id' => 2,
                'title' => 'Cover Truck',
				'title_bn' => 'কভার ট্রাক',
                'capacity' => 12000,
                'max_capacity' => 12000,
                'capacity_type_id' => 1,
                'icon_url' => 'public/vehicle_types/cover_truck.png'
            ],
            [   
                'id' => 3,
                'title' => 'Pickup Van',
				'title_bn' => 'পিকআপ ভ্যান',
                'capacity' => 5000,
                'max_capacity' => 5000,
                'capacity_type_id' => 1,
                'icon_url' => 'public/vehicle_types/pickup_van.png'
            ],
            [
                'id' => 4,
                'title' => 'Mini Pickup Van',
				'title_bn' => 'মিনি পিকআপ ভ্যান',
                'capacity' => 2000,
                'max_capacity' => 2000,
                'capacity_type_id' => 1,
                'icon_url' => 'public/vehicle_types/mini_pickup_van.png'
            ],
            [
                'id' => 5,
                'title' => 'Tanker Truck',
				'title_bn' => 'ট্যাঙ্কার ট্রাক',
                'capacity' => 5000,
                'max_capacity' => 5000,
                'capacity_type_id' => 2,
                'icon_url' => 'public/vehicle_types/tanker_truck.png'
            ],
            [
                'id' => 6,
                'title' => 'Bus',
				'title_bn' => 'বাস',
                'capacity' => 36,
                'max_capacity' => 36,
                'capacity_type_id' => 4,
                'icon_url' => 'public/vehicle_types/bus.png'
            ],
            [
                'id' => 7,
                'title' => 'Mini Bus',
				'title_bn' => 'মিনি বাস',
                'capacity' => 25,
                'max_capacity' => 25,
                'capacity_type_id' => 4,
                'icon_url' => 'public/vehicle_types/mini_bus.png'
            ],
            [
                'id' => 8,
                'title' => 'Micro Bus',
				'title_bn' => 'মাইক্রো বাস',
                'capacity' => 10,
                'max_capacity' => 10,
                'capacity_type_id' => 4,
                'icon_url' => 'public/vehicle_types/micro_bus.png'
            ],
            [
                'id' => 9,
                'title' => 'Ambulance',
				'title_bn' => 'অ্যাম্বুলেন্স',
                'capacity' => 3,
                'max_capacity' => 3,
                'capacity_type_id' => 4,
                'icon_url' => 'public/vehicle_types/ambulance.png'
            ],
            [
                'id' => 10,
                'title' => 'Construction vehicle',
				'title_bn' => 'কন্সট্রাকশান বেহিক্যাল',
                'capacity' => 6,
                'max_capacity' => 6,
                'capacity_type_id' => 3,
                'icon_url' => 'public/vehicle_types/construction_vehicle.png'
            ],
            [
                'id' => 11,
                'title' => 'Special Truck',
				'title_bn' => 'স্পেশাল ট্রাক',
                'capacity' => 19000,
                'max_capacity' => 19000,
                'capacity_type_id' => 1,
                'icon_url' => 'public/vehicle_types/special_truck.png'
            ],
            [
                'id' => 12,
                'title' => 'Mini Truck',
				'title_bn' => 'মিনি ট্রাক',
                'capacity' => 8000,
                'max_capacity' => 8000,
                'capacity_type_id' => 1,
                'icon_url' => 'public/vehicle_types/mini_truck.png'
            ],
            [
                'id' => 13,
                'title' => 'Freezer Truck',
				'title_bn' => 'ফ্রিজার ট্রাক',
                'capacity' => 8000,
                'max_capacity' => 8000,
                'capacity_type_id' => 1,
                'icon_url' => 'public/vehicle_types/freezer_truck.png'
            ],
            [
                'id' => 14,
                'title' => 'Coaster Bus',
				'title_bn' => 'কোস্টার বাস',
                'capacity' => 18,
                'max_capacity' => 18,
                'capacity_type_id' => 4,
                'icon_url' => 'public/vehicle_types/coaster_bus.png'
            ],
            [
                'id' => 15,
                'title' => 'Bus (AC)',
				'title_bn' => 'বাস  ( এসি )',
                'capacity' => 36,
                'max_capacity' => 36,
                'capacity_type_id' => 4,
                'icon_url' => 'public/vehicle_types/bus_ac.png'
            ],
            [
                'id' => 16,
                'title' => 'Trailer Truck 2XL',
				'title_bn' => 'কোনটেইনার ট্রাক 2XL',
                'capacity' => 20,
                'max_capacity' => 20,
                'capacity_type_id' => 5,
                'icon_url' => 'public/vehicle_types/container_truck_20.png'
            ],
            [
                'id' => 17,
                'title' => 'Trailer Truck 3XL',
				'title_bn' => 'কোনটেইনার ট্রাক 3XL',
                'capacity' => 40,
                'max_capacity' => 40,
                'capacity_type_id' => 5,
                'icon_url' => 'public/vehicle_types/container_truck_40.png'
            ],
            [
                'id' => 18,
                'title' => 'Car',
				'title_bn' => 'গাড়ী ',
                'capacity' => 3,
                'max_capacity' => 3,
                'capacity_type_id' => 4,
                'icon_url' => 'public/vehicle_types/car.png'
            ],
            [
                'id' => 19,
                'title' => 'Mini Micro Bus',
                'title_bn' => 'মিনি মাইক্রো বাস',
                'capacity' => 7,
                'max_capacity' => 7,
                'capacity_type_id' => 4,
                'icon_url' => 'public/vehicle_types/64S5ikMlfDRsKCiVZzQLUTwbpqVbbJerDqj95m71.png'
            ],
            [
                'id' => 20,
                'title' => 'Bus (Business Class)',
                'title_bn' => 'বাস (বিজিনেস ক্লাস) ',
                'capacity' => 27,
                'max_capacity' => 27,
                'capacity_type_id' => 4,
                'icon_url' => 'public/vehicle_types/PZ6AuL8s2YGpy8Es8rMG2OTXUmqaYQR6RACWF4PI.png'
            ]
        ];

        foreach ($vehicle_types as $key => $type) {
            VehicleType::updateOrCreate( $type );
        }
    }
}
