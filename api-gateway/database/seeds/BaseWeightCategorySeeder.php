<?php

use Illuminate\Database\Seeder;
use App\BaseWeightCategory;

class BaseWeightCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertWeightCategories();
        $this->command->info('Base weight categories seeded!');
    }

    protected function insertWeightCategories()
    {
        $weight_categories = [
            [
                'title' => 'Below 1 Kg',
                'weight' => '1'
            ],
            [
                'title' => '1 to 5 kg',
                'weight' => '5'
            ],
            [
                'title' => '5 to 10 kg',
                'weight' => '10'
            ]
        ];

        foreach ($weight_categories as $key => $category) {
            BaseWeightCategory::updateOrCreate( $category );
        }
    }
}
