<?php
namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\VehicleType;

class BaseController extends ApiController
{
    public function getVehicleTypes(){
        $vehicle_types = VehicleType::get();
        return $this->reformCollection($vehicle_types);
    }
}