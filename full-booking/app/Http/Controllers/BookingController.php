<?php
namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Libraries\BookingLib\BookingManager;
use App\ProfileCustomer;

class BookingController extends ApiController
{
    public function getCost( Request $request ){
        $booking_fields = $request->all();
        $manager = new BookingManager('full');
        $price_breakdown = $manager->getFare($booking_fields);
        if(!$price_breakdown)
            return $this->respondServerError('Sorry! Could not calcualte the cost.');

        return $price_breakdown;
    }

    public function create( Request $request, $id ){
        $customer = ProfileCustomer::where('id', $id)->first();
        $booking_fields = $request->all();

        $manager = new BookingManager('full');
        $manager->setBookingCustomer($customer);
        $booking = $manager->createBooking($request->all());
        
        if (!$booking) {
            return $this->respondServerError('Sorry! Could not do the booking.');
        }
        
        return [ 'message' => 'Booking was created', 'booking_id' => $booking['unique_id'] ];
    }
}