<?php

namespace App\Libraries\BookingLib\CostCalculator;

use App\TripCategory;
use App\VehicleType;

class FullBookingCalculator extends FareCalculator
{
	private $distance;
	private $duration;
	private $distance_type;
	private $trip_category;
	private $vehicle_fullbooking_rate;

	public function __construct($duration, $distance, $distance_type, $trip_type, $trip_category, $vehicle_type)
	{
		$this->distance = $distance;
		$this->duration = $duration;
		$this->distance_type = $distance_type;
		$this->trip_type = $trip_type;
		$this->trip_category = $trip_category;
		$this->vehicle_fullbooking_rate = VehicleType::where('id', $vehicle_type)->first()->fullBookingRate;
	}

	/**
	 * Calculate Base Fare
	 */
	protected function getBaseFare()
	{
		return $base_fare = ($this->distance_type=='short') ? $this->vehicle_fullbooking_rate['base_fare'] : 0;
	}

	/**
	 * Calculate Distance Cost
	 */
	protected function getDistanceCost()
	{
		$distance_cost = $this->getPerKiloRate() * $this->distance;
		return round($distance_cost);
	}

	/**
	 * Calculate waiting Cost
	 */
	protected function getTimeCost(){
		$time_cost = 0;

		if($this->distance_type=='normal' || $this->distance_type=='long'){
			return $time_cost;
		}

		$time_cost = $this->duration / 60;
		if ($this->trip_type=='round') {
			$time_cost *= 2;
		}
		return round($time_cost);
	}

	/**
	 * Get the Per Kilo meter Rate
	 * 
	 * FULL DISCUSS GOES HERE...
	 */
    protected function getPerKiloRate()
    {
		if ($this->distance_type=='short') {
			$per_km_rate = $this->vehicle_fullbooking_rate['short_trip_rate'];
			if ($this->trip_type=='round') {
				$per_km_rate *= 2;
			}
		} else if ($this->distance_type=='normal') {
			$per_km_rate = ($this->trip_type=='round') ? ( $this->vehicle_fullbooking_rate['up_trip_rate'] * $this->trip_category['up_trip_multiplier'] + ($this->vehicle_fullbooking_rate['down_trip_rate'] * $this->trip_category['down_trip_multiplier'] * .30)  ) : $this->vehicle_fullbooking_rate[$this->trip_category['category'].'_trip_rate'];
		} else if ($this->distance_type=='long') {
			$per_km_rate = ($this->trip_type=='round') ? ( $this->vehicle_fullbooking_rate['long_up_trip_rate'] * $this->trip_category['up_trip_multiplier'] + ($this->vehicle_fullbooking_rate['long_down_trip_rate'] * $this->trip_category['down_trip_multiplier'] * .30) ) : $this->vehicle_fullbooking_rate['long_'.$this->trip_category['category'].'_trip_rate'];
		}

		// is there any special multiplier for that route?
		if ($this->distance_type!='short' && $this->trip_type!='round') {
			$per_km_rate =  $per_km_rate * $this->trip_category['multiplier'];
		}

		return round($per_km_rate);
    }

    /**
	 * Calculate Surcharge
	 */
	protected function getSurchargeCost()
	{
		$surcharge = $this->vehicle_fullbooking_rate['surcharge_rate'] * $this->distance;
		return round($surcharge);
	}

	/**
	 * Calculate HeavyGari Fee
	 */
	protected function getHeavyGariFee($total)
	{
		$heavygari_percent = $this->vehicle_fullbooking_rate['admin_commission'];
		$heavygari_fee = $total * ($heavygari_percent/100);
		return round($heavygari_fee);
	}

    /**
	 * Calculate Discount
	 */
	protected function getDiscountAmount($total_fare)
	{
		$discount_percent = $this->vehicle_fullbooking_rate['discount_percent'];
		$discount = $total_fare * ($discount_percent/100);
		return round($discount);
	}
}