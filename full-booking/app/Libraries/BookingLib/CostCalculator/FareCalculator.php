<?php

namespace App\Libraries\BookingLib\CostCalculator;


abstract class FareCalculator {

	
	/**
	 * Get Full Costing Breakdown of a Shared Booking
     *
     * details : 
     *	 
	 */
	final public function getFullCostBreakDown()
	{
		$invoice_breakdown = $this->getFareBreakDown();
		
		# Earnings
		//$driver_percent = config('heavygari.driver_comission');
		//$settings = Settings::where('key', 'driver_comission')->first();
    	$driver_percent = .5;

		$owner_earning = $invoice_breakdown['total_fare'] - $invoice_breakdown['heavygari_fee'];
		$heavygari_fee = $invoice_breakdown['heavygari_fee'];
		$driver_earning = $heavygari_fee * $driver_percent; 	
		$heavygari_earning = $heavygari_fee - $driver_earning;

		$cost_breakdown = [
			'invoice' => $invoice_breakdown,
			'earnings' => [
				'owner_earning' => (float) $owner_earning,
				'heavygari_earning' => (float) $heavygari_earning,
				'driver_earning' => (float) $driver_earning,
			]
		];

		return $cost_breakdown;
	}
	

	/**
	 * Get Fare Brakedown
     *
     * details : 
     *	 
	 */
	final public function getFareBreakDown()
	{
	    $base_fare = $this->getBaseFare();
		$pickup_charge = $this->getPickupCharge();
		$dropoff_charge = $this->getDropOffCharge();
		$distance_cost = $this->getDistanceCost();
		$weight_cost = $this->getWeightCost();
		$surcharge = $this->getSurchargeCost();
		$time_cost = $this->getTimeCost();
        //---------------------------------------------
        $total = $base_fare + $pickup_charge + $dropoff_charge + $distance_cost + $weight_cost + $surcharge + $time_cost;
        $heavygari_fee = $this->getHeavyGariFee($total);
        //---------------------------------------------
		$total_fare = $total + $heavygari_fee;
		$discount = $this->getDiscountAmount($total_fare);
        //---------------------------------------------------
		$total_cost = $total_fare - $discount;

		$cost_breakdown = [
			'pickup_charge' => (float) $pickup_charge,
			'dropoff_charge' => (float) $dropoff_charge,
			'base_fare' => (float) $base_fare,
			'distance_cost' => (float)$distance_cost,
			'weight_cost' => (float) $weight_cost,
			'surcharge' => (float) $surcharge,
			'time_cost' => (float) $time_cost,
			'heavygari_fee' => (float) $heavygari_fee,
			'total_fare' => (float) $total_fare,
			'discount' => (float) $discount,
			'total_cost' => (float) $total_cost
		];

		return $cost_breakdown;
	}

	/**
	 * This methods must be implemented
	 */
	abstract protected function getBaseFare();
	abstract protected function getDistanceCost();
	abstract protected function getSurchargeCost();
	abstract protected function getTimeCost();
	abstract protected function getDiscountAmount($total_fare);

	/**
	 * Optional methods (for parcel booking)
	 */
	protected function getPickupCharge()
	{
	    return 0;
	}

	protected function getDropOffCharge()
	{
	    return 0;
	}

	protected function getWeightCost()
	{
	    return 0;
	}
}