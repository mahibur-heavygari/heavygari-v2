<?php

namespace App\Libraries\BookingLib;

use App\Booking;
use Carbon\Carbon;
use App\ProfileCustomer;
//use App\Events\Booking\NewBookingCreated;

class BookingManager
{
	public $booking;
    public $category;
	public $customer;

	public function __construct($category) 
    {
    	if ($category == 'full') {
            $this->category = 'full';
    		$this->booking = new FullBooking();
    	}
    }

	public function setBookingCustomer(ProfileCustomer $customer) 
    {
    	$this->customer = $customer;
    }

    public function getFare($booking_info) 
    {
        $cost = $this->booking->getEstimatedFare($booking_info);
        return $cost;
    }

    public function createBooking($booking_info) 
    {
        $booking_info = $this->prepareFields($booking_info);
        $booking = $this->booking->create($booking_info);
        return $booking;
    }

    private function prepareFields($booking_fields)
    {
        $booking_fields['profile_customer_id'] = $this->customer->id;
        $booking_fields['unique_id'] = $this->getUniqueId();
        return $booking_fields;
    }

    private function getUniqueId()
    {
        $last_id = Booking::all()->last() ? Booking::all()->last()->id : 0;
        $id = (int) $last_id + 1;
        $postfix = ($this->category=='shared') ? 'S' : 'F';
        return 'HG'.'-'.$id.$postfix;
    }    
}