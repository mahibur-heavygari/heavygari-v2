<?php

namespace App\Libraries\BookingLib;


interface BookingInterface
{
	public function getEstimatedFare($booking_info);

	public function create($booking_info);
}