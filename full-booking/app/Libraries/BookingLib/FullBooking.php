<?php

namespace App\Libraries\BookingLib;

use App\Booking;
use App\TripCategory;
use App\BasePoint;
use App\VehicleType;
use App\Libraries\GoogleMapLib\GoogleMapFacade;
use App\Libraries\BookingLib\CostCalculator\FullBookingCalculator;
use Illuminate\Support\Arr;
use DB;

class FullBooking implements BookingInterface
{	
	private $booking_info;
	private $trip_category;
	private $distance_type;
	private $total_distance = 0;
	private $total_duration = 0;
	private $multiplier = null;
	private $merged_points = [];
	private $point_details = [];
	private $trips = [];
	private $customer;

	/**
	 * Determine and set some attributes
	 */
	public function setter($booking_info)
	{
		$this->booking_info = $booking_info;

		$this->mergePoints();

		$points_res = $this->getAllPointDetails();
		if($points_res == false)
    		return false;

		$this->getTotalTrips();

		$this->setDistanceType();

		$this->setTripCategory();

		return true;
	}
	
	/**
	 * Get Estimated Fare
	 *
	 * @return Array
	 */
	public function getEstimatedFare($booking_info)
	{
		$setter_res = $this->setter($booking_info);
		if( !$setter_res )
			return false;

		$calculator = new FullBookingCalculator($this->total_duration, $this->total_distance, $this->distance_type,  $this->booking_info['trip_type'], $this->trip_category, $this->booking_info['vehicle_type']);
		$fare_breakdown = $calculator->getFareBreakDown();

		$fare_breakdown = [
			'fare' => [
				'total_cost' => $fare_breakdown['total_cost'],
				'fare_breakdown' => $fare_breakdown,
			],			
	        'route' => [
	        	'total_distance' => number_format($this->total_distance, 2),
	        	'total_duration' => $this->secondToHour($this->total_duration),
	        	'total_duration_in_second' => $this->total_duration,
	        	'distance_type' => $this->distance_type,
       			'category' => $this->trip_category,
	        	'point_details' => $this->point_details,
	        	'trips' => $this->trips
	        ]
		];
		return $fare_breakdown;
	}

	private function mergePoints()
	{
		$this->merged_points[] = $this->booking_info['from'];
		foreach ($this->booking_info['to'] as $key => $value) {
			$this->merged_points[] = $value;
		}
	}

    private function getBasePoint( $locations )
    {
    	$point = NULL;
    	foreach ($locations as $key => $location) {
    		$point = BasePoint::where('title', $location)->first();
    		if(!is_null($point))
    			break;
    	}
    	return $point;
    }

	private function getPointDetails( $point )
    {
        $address_details = GoogleMapFacade::getLocationInfo($point['lat'].','.$point['lon']);
        $locations = $this->furnishLocations( $address_details );
        $point_details = $this->getBasePoint( $locations );
        return $point_details;
    }

    private function getAllPointDetails(){
    	$merged_points = $this->merged_points;

    	foreach ($merged_points as $key => $value) {
    		$details = $this->getPointDetails( $value );
    		if(is_null($details))
    			return false;
    		$this->point_details[] = [
                'address' => $value['address'],
                'point' => $details->id,
                'point_title' => $details->title,
                'coordinators' => [
                    'lat' => (float) $value['lat'],
                    'lon' => (float) $value['lon']
	            ]
            ];
    	}
    	return true;
    }

	private function getDistanceDuration( $from_location, $to_location )
	{
        $distance_duration = GoogleMapFacade::getDistanceWithDuration($from_location['coordinators']['lat'].','.$from_location['coordinators']['lon'] , $to_location['coordinators']['lat'].','.$to_location['coordinators']['lon'] );
        return $distance_duration;
	}

	private function getTotalTrips()
	{
		$point_details = $this->point_details;

		for ($i=0; $i < (count($point_details) - 1 ); $i++) { 
			
			$from_location = $point_details[$i];
			$to_location = $point_details[$i+1];

			$distance_duration = $this->getDistanceDuration( $from_location, $to_location );
			$distance = number_format((float) $distance_duration['distance'], 2);
			$duration = $distance_duration['duration'];

	        $this->total_distance += $distance;
	        $this->total_duration += $duration;

	        $this->trips[] = [
                'from_address' => $from_location['address'],
                'from_point' => $from_location['point'],
                'from_lat' => $from_location['coordinators']['lat'],
                'from_lon' => $from_location['coordinators']['lon'],
                'to_address' => $to_location['address'],
                'to_point' => $to_location['point'],
                'to_lat' => $to_location['coordinators']['lat'],
                'to_lon' => $to_location['coordinators']['lon'],
                'distance' => $distance,
                'duration' => $this->secondToHour($duration),
                'duration_in_second' => $duration
            ];
		}
	}

    private function furnishLocations( $locations )
    {
    	krsort($locations);
    	$flattened = Arr::flatten($locations);
    	$flattened = array_unique($flattened);
    	$formated = str_replace([" Paurashava", " Pouroshova", " Upazila", " Sadar", " Sadar Upazila", " District", " Division"], "", $flattened) ;
    	$formated = array_unique($formated);

    	return $formated;
    }

    /**
	 * Determine and set the Distance Type (short, normal..)
	 */
	private function setDistanceType()
	{
		$distance_type = 'normal';

		//$short_distance = config('heavygari.distance.full_booking.short');
		//$long_distance = config('heavygari.distance.full_booking.long');
		$short_distance = 120;
		$long_distance = 400;

		if ($this->total_distance < $short_distance) {
			$distance_type = 'short';
		} else if ($this->total_distance > $long_distance) {
			$distance_type = 'long';
		}

		$this->distance_type = $distance_type;
	}

	/**
	 * Determine and set the Trip Type (up/down)
	 */
	private function setTripCategory()
	{
		$source_point = $this->point_details[0]['point'];
		$destination_point = end($this->point_details)['point'];

		if($this->booking_info['trip_type']=='single'){			
			$trip_category = [
				'category' => 'up',
				'multiplier' => 1
			];

			$trip = TripCategory::where(['src_point_id' => $source_point, 'dest_point_id' => $destination_point])->first();

			if ( $trip ) {
				$multiplier = $this->getMultiplier( $trip->id );
				$trip_category = [
					'category' => $trip['trip_category'],
					'multiplier' => $multiplier
				]; 
			}

		}else{
			$trip_category = [
				'category' => 'round',
				'up_trip_multiplier' => 1,
				'down_trip_multiplier' => 1,
			];

			$departure_trip = TripCategory::where(['src_point_id' => $source_point, 'dest_point_id' => $destination_point])->first();
			$arrival_trip = TripCategory::where(['src_point_id' => $destination_point, 'dest_point_id' => $source_point])->first();

			if ($departure_trip && $departure_trip->trip_category=='up') {
				$trip_category['up_trip_multiplier'] = $this->getMultiplier( $departure_trip->id );
			}elseif ($departure_trip && $departure_trip->trip_category=='down') {
				$trip_category['down_trip_multiplier'] = $this->getMultiplier( $departure_trip->id );
			}

			if ($arrival_trip && $arrival_trip->trip_category=='up') {
				$trip_category['up_trip_multiplier'] = $this->getMultiplier( $arrival_trip->id );
			}elseif ($arrival_trip && $arrival_trip->trip_category=='down') {
				$trip_category['down_trip_multiplier'] = $this->getMultiplier( $arrival_trip->id );
			}
		}

		$this->trip_category = $trip_category;
	}

	//set multiplier for spcific route and vehicle
	private function getMultiplier( $id )
	{
		return 1;
	}

	private function secondToHour($seconds) {
		$hours = floor($seconds / 3600);
		$minutes = floor(($seconds / 60) % 60);
		$seconds = $seconds % 60;
		return $hours > 0 ? "$hours hours, $minutes minutes" : ($minutes > 0 ? "$minutes minutes" : "");
	}

	public function create( $booking_info )
	{
		try {
			$this->setter($booking_info);

			// get price breakdown 
			$calculator = new FullBookingCalculator($this->total_duration, $this->total_distance, $this->distance_type, $this->booking_info['trip_type'], $this->trip_category, $this->booking_info['vehicle_type']);
			$cost_breakdown = $calculator->getFullCostBreakDown();

			$booking_info['booking_category'] = 'full';
	 		$booking_info['total_distance'] = $this->total_distance;
	 		$booking_info['total_duration'] = $this->total_duration;
	 		$booking_info['multiplier'] = $this->multiplier;
	        $booking_info['distance_type'] = $this->distance_type;
	        $booking_info['vehicle_type_id'] = $this->booking_info['vehicle_type'];
			$booking_info['capacity'] = $this->getVehicleCapacity();

	        $db_records['booking'] = $booking_info; 
	        $db_records['invoice'] = $cost_breakdown['invoice'];
	        $db_records['earnings'] = $cost_breakdown['earnings'];
	        $db_records['trips'] = $this->trips;

	        // insert into database tables
	        DB::beginTransaction();

	       	$booking = Booking::create($db_records['booking']);      
	        $full_booking_details = $booking->fullBookingDetails()->create($db_records['booking']); 
	        $invoice = $booking->invoice()->create($db_records['invoice']);
	        $earnings = $booking->earnings()->create($db_records['earnings']);
	        foreach ($db_records['trips'] as $key => $value) {
	        	$db_records['trips'][$key]['booking_id'] = $booking->id;
	        }
	        $trip = $booking->trips()->insert($db_records['trips']);

	        DB::commit();
        } catch(\Exception $e) {
        	
        	// Todo : log this?
	    	DB::rollBack();

	    	return false;
	    }
        return $booking;
	}

	private function getVehicleCapacity() {
		$vehicle_type = VehicleType::find($this->booking_info['vehicle_type']);
		$vehicle_capacity_type = $vehicle_type->capacityType;
		$capacity = $vehicle_type->capacity.' '.$vehicle_capacity_type->title;
		return $capacity;
	}
}

//$distance_duration = GoogleMapFacade::getLocationInfo('23.8694867,91.2035606'); //akhaura
//$distance_duration = GoogleMapFacade::getLocationInfo('24.2733156,91.4685042'); //shaistagonj puran bazar
//$distance_duration = GoogleMapFacade::getLocationInfo('24.0447785,90.9826861'); //bhairab bazar
//$distance_duration = GoogleMapFacade::getLocationInfo('23.9375019,90.6238494'); //ghorasal
//$distance_duration = GoogleMapFacade::getLocationInfo('23.254613,91.1218655');  //laksam
//$distance_duration = GoogleMapFacade::getLocationInfo('23.8938531,90.4013158'); //Tongi bus stop
//$distance_duration = GoogleMapFacade::getLocationInfo('25.5834743,89.3468713'); //Balarhat
//$distance_duration = GoogleMapFacade::getLocationInfo('24.1303007,89.0608231'); //Ishwardi
//$distance_duration = GoogleMapFacade::getLocationInfo('24.8005623,88.9729928');   //Santahar
//$distance_duration = GoogleMapFacade::getLocationInfo('21.4166318,91.9810957');   //Cox's Bazar Bus Terminal, Cox's Bazar
//Pouroshova, Upazila, District, Sadar Upazila