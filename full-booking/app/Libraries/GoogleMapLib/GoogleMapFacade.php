<?php

namespace App\Libraries\GoogleMapLib;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class GoogleMapFacade
{
	/* Get distance of two address */

	public static function getDistanceWithDuration($from, $to)
	{
		$distance = 0;
		$duration = 0;
		$distanceURL = 'https://maps.googleapis.com/maps/api/distancematrix/json?key='.config('heavygari.google_maps.api_key').'&origins='.$from.'&destinations='.$to;

		$client = new Client();
		$res = $client->get($distanceURL);
		$output = json_decode($res->getBody()->getContents());

		if(isset($output->rows[0]->elements[0]->distance->value)) {
			$distance =  ($output->rows[0]->elements[0]->distance->value)/1000;
		}
		if(isset($output->rows[0]->elements[0]->duration->value)) {
			$duration =  $output->rows[0]->elements[0]->duration->value;
		}

		return array('distance' => $distance, 'duration' => $duration);
	}

	public static function getLocationInfo($from)
	{
		$distance = 0;
		$duration = 0;
		$locationUrl = 'https://maps.googleapis.com/maps/api/geocode/json?key='.config('heavygari.google_maps.api_key').'&language=english&latlng='.$from;

		$client = new Client();
		$response = $client->get($locationUrl);
		$output = json_decode($response->getBody()->getContents());
		//dd($output);

		$results = $output->results;
		$locations = [];
		
		foreach ($results as $result_key => $result) {
			foreach ($result->address_components as $key => $address_component) {
				if($address_component->types[0] == 'administrative_area_level_1')
					$locations[1][] = $address_component->long_name;
				if($address_component->types[0] == 'administrative_area_level_2'){
					$locations[2][] = $address_component->long_name;
				}
				if($address_component->types[0] == 'administrative_area_level_3')
					$locations[3][] = $address_component->long_name;
				if($address_component->types[0] == 'administrative_area_level_4')
					$locations[4][] = $address_component->long_name;
				if($address_component->types[0] == 'administrative_area_level_5')
					$locations[5][] = $address_component->long_name;
				if( (isset($address_component->types[0]) && $address_component->types[0] == 'locality') || ( isset($address_component->types[1]) && $address_component->types[1] == 'locality'))
					$locations[6][] = $address_component->long_name;
			}
		}

		return $locations;
	}

}