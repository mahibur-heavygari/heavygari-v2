<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingEarning extends Model
{
    protected $table = 'full_booking_earnings';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'booking_id', 'owner_earning', 'driver_earning', 'heavygari_earning'
    ];

    ///////////////
    // RELATIONS //
    ///////////////
    
    /**
     * Get parent booking 
     */
    public function booking()
    {
        return $this->belongsTo('App\Booking', 'booking_id');
    }
}
