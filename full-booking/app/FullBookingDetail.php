<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FullBookingDetail extends Model
{
    protected $table = 'full_booking_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['capacity', 'trip_type', 'invoice_pdf', 'product_category_id', 'product_category_text', 'waiting_time', 'recipient_name', 'recipient_phone', 'payment_by', 'is_payment_collected', 'special_instruction', 'multiplier', 'distance_type', 'total_distance', 'total_duration', 'created_by', 'accepted_at', 'accepted_by', 'started_at', 'started_by', 'completed_at', 'completed_by', 'cancelled_at', 'cancelled_by', 'cancelled_reason', 'accept_time_profile_driver_id', 'start_time_profile_driver_id', 'complete_time_profile_driver_id', 'cancel_time_profile_driver_id'];

    ///////////////
    // RELATIONS //
    ///////////////

    public function booking()
    {
        return $this->belongsTo('App\Booking', 'booking_id');
    }

    public function vehicleType()
    {
        return $this->belongsTo('App\VehicleType');
    }

    public function getApiModel()
    {
        return [
            'vehicle_type' => $this->vehicleType->getApiModel(),
            'capacity' => $this->capacity
        ];
    }
}
