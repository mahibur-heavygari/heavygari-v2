<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{    
    protected $table = 'full_bookings';

    protected $fillable = ['unique_id', 'profile_customer_id', 'profile_owner_id', 'profile_owner_manager_id', 'profile_driver_id', 'profile_vehicle_id', 'vehicle_type_id', 'datetime','booking_category', 'booking_type', 'status'];


    public function fullBookingDetails()
    {
        return $this->hasOne('App\FullBookingDetail', 'booking_id');
    }

    public function invoice()
    {
        return $this->hasOne('App\BookingInvoice', 'booking_id');
    }

    public function earnings()
    {
        return $this->hasOne('App\BookingEarning', 'booking_id');
    }
    
    public function trips()
    {
        return $this->hasMany('App\Trip', 'booking_id');
    }
}
