<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use URL;

class ProfileCustomer extends Model
{
	protected $table='base_profile_customers';
	
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
