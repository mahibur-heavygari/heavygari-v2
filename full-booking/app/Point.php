<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    protected $table = 'base_points';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'fullname', 'lat', 'lon'
    ];

    /////////////////
    // API Models //
    ////////////////

    public function getApiModel()
    {
        return [
            'id'=> $this->id,
            'title'=> $this->title,
            'lat' => (float) $this->lat,
            'lon' => (float) $this->lon,
        ];
    }
}
