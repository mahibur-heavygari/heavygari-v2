<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleCapacityType extends Model
{
    protected $table = 'base_vehicle_capacity_types';

    protected $fillable = [
        'title'
    ];

    public function vehicleTypes()
    {
        return $this->hasMany('App\VehicleType', 'capacity_type_id');
    }
}
