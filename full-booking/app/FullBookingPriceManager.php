<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FullBookingPriceManager extends Model
{
    protected $table = 'full_booking_price_manager';

    protected $fillable = [
        'base_fare', 'vehicle_type_id', 'short_trip_rate', 'up_trip_rate', 'down_trip_rate', 'long_up_trip_rate', 'long_down_trip_rate', 'base_fare', 'surcharge_rate', 'discount_percent', 'admin_commission'
    ];
}
