<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use URL;

class VehicleType extends Model
{
    protected $table = 'base_vehicle_types';

    protected $fillable = [
        'title', 'title_bn', 'capacity_type_id', 'capacity', 'max_capacity', 'icon_url'
    ];  

    
    public function fullBookingRate()
    {
        return $this->hasOne('App\FullBookingPriceManager', 'vehicle_type_id');
    } 

    public function capacityType()
    {
        return $this->belongsTo('App\VehicleCapacityType', 'capacity_type_id');
    } 

    public function getApiModel()
    {
        //substr is used here to remove 'public/' string from icon_url column
        $icon_url = isset($this->icon_url) ? URL::to(substr($this->icon_url, 7)) : URL::to('vehicle_types/default.png');

        return [
            'id'=> $this->id,
            'title'=> $this->title,
            'title_bn'=> $this->title_bn,
            'capacity'=> $this->capacity,
            'max_capacity'=> $this->max_capacity,
            'icon_url'=> $icon_url,
            'capacity_type'=> [
                'id'=> $this->capacityType->id,
                'title'=> $this->capacityType->title,
            ],
            'pricing'=> [
                'base_fare'=> $this->fullBookingRate['base_fare'],
                'short_trip_rate'=> $this->fullBookingRate['short_trip_rate'],
                'up_trip_rate'=> $this->fullBookingRate['up_trip_rate'],
                'down_trip_rate'=> $this->fullBookingRate['down_trip_rate'],
                'long_up_trip_rate'=> $this->fullBookingRate['long_up_trip_rate'],
                'long_down_trip_rate'=> $this->fullBookingRate['long_down_trip_rate'],
            ]
        ];
    }
}
