<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Google Maps
    |--------------------------------------------------------------------------
    |
    | Here, you may configure Google Maps settings
    |
    */

    'google_maps' => [

        'api_key' => getenv('GOOGLE_MAPS_API_KEY') ?: ''

    ],

    /*
    |--------------------------------------------------------------------------
    | Firebase
    |--------------------------------------------------------------------------
    |
    | Here, you may configure Firebase settings
    |
    */

    'firebase' => [

        'fcm_server_key' => getenv('FIREBASE_CLOUD_MESSAGING_SK') ?: '',
        'web_fcm_server_key' => getenv('FIREBASE_CLOUD_MESSAGING_SK_WEB') ?: ''

    ],

    /*
    |--------------------------------------------------------------------------
    | Distances
    |--------------------------------------------------------------------------
    |
    | Here, you may configure distances
    |
    */

    'distance' => [

        'full_booking' => [
            'short' => 120,
            'long' => 400
        ],
        'shared_booking' => [
            'shortest' => 3,
            'short' => 15,
            'long' => 400
        ]      

    ],

    /*
    |--------------------------------------------------------------------------
    | Commission
    |--------------------------------------------------------------------------
    |
    | Here, you may configure commissions
    |
    */

    'driver_comission' => .5,

];
