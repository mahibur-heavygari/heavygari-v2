<?php

namespace App\Libraries\SMSLib;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\GuzzleException;
use App\SmsHistory;


class SMSFacade
{
	public static function send($to, $message, $ref_id)
	{	
		return true;
		
		$to = self::formatNumberProperly($to);
		$credentials = self::getCredentials();
		
		$client = new Client();
		$unicode_msg = mb_strtoupper(bin2hex(mb_convert_encoding($message, 'UTF-16BE', 'UTF-8')));

		try{
			$response = $client->request('POST', $credentials['url'], [
			    'form_params' => [
			        'user' => $credentials['user'],
		    		'pass' => $credentials['pass'],
		    		'sid' => $credentials['sid'],
		    		'sms[0][0]' => $to,
		    		'sms[0][1]' => $unicode_msg,
		    		'sms[0][2]' => $ref_id	    		
			    ]
			]);
		} catch(\Exception $e) {
	    	return false;
	    }

		//Log::info('Trying to Send SMS. To :'.$to.', Message : '.$message.' Response : '. var_export($response, true));

		$xml = simplexml_load_string($response->getBody()->getContents());
		$json = json_encode($xml);

		$array = json_decode($json, TRUE);
		if (isset($array['SMSINFO']['REFERENCEID'])) {
			return true;
		}		
		return false;
	}

	private static function getCredentials()
	{
		return [
			'url' => env('SSL_URL'),
			'user' => env('SSL_USER'),
			'pass' => env('SSL_PASSWORD'),
			'sid' => env('SSL_SID'),
		];
	}

	public static function formatNumberProperly($phone)
	{
		// remove '+' sign if any
		$phone = ltrim($phone, '+');

		// if 88 is not present add it.
		$phone = substr( $phone, 0, 2 ) === "88" ? $phone : '88'.$phone;

		return $phone;
	}

}