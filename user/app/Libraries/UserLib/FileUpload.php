<?php

namespace App\Libraries\UserLib;

use Illuminate\Http\Request;
use URL;

class FileUpload
{
	public static function uploader( Request $request, $storagePath )
    {
        $original_name = $request->file('file')->getClientOriginalName();
        $exploded = explode('.', $original_name);
        $extention = array_pop($exploded);
        
        $uniq_name = uniqid('file').mt_rand(1000000000, 9999999999).'.'.$extention;
        $request->file('file')->move(base_path('public').$storagePath, $uniq_name);

        return [
            'path' => $storagePath.'/'.$uniq_name,
        ];
    }
}