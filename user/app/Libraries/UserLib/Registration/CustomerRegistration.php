<?php

namespace App\Libraries\UserLib\Registration;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Libraries\SMSLib\SMSFacade;
use App\User;
use App\ProfileCustomer;
use App\PhonePin;
use App\Role;

class CustomerRegistration
{
	protected $user;
	protected $user_info;

	public function __construct($user_info)
	{
		$this->user_info = $user_info;
	}

	public function registerCustomer()
	{
		$customer = $this->registerUser();
		if($customer == false)
			return false;

		return $this->user->profileCustomer;
	}

	protected function registerUser()
	{
		$user_data = [
		    'username' => $this->user_info['phone'],
		    'name' => $this->user_info['name'],
		    'password' => Hash::make($this->user_info['password']),
		    'phone' => $this->user_info['phone'],
		    'email' => $this->user_info['email'],
		    'plain_password' => $this->user_info['password']
		];

		try {
			DB::beginTransaction();

			$this->user = User::create($user_data);
			$this->createProfile();
			$this->addRole();

		} catch (\Exception $e) {
			DB::rollBack();
			$this->error = $e;
			return false;
		}
		DB::commit();

		return true;
	}

	public function createProfile()
	{
		$this->user->status = 'active';
		$this->user->save();
		
		$profile = new ProfileCustomer;
		$profile->user_id = $this->user->id;
		$profile->profile_complete = 'yes';
		$profile->save();
	}

	public function addRole()
	{
		$role = isset($this->user_info['role']) ? $this->user_info['role'] : 'customer_regular';

		$role = Role::where('code', $role)->first();
        $this->user->roles()->attach($role);
        $permissions = $role->permissions;

        $this->user->permissions()->attach($permissions);
	}

	public function checkPin()
	{
		$res = PhonePin::where('phone', $this->user_info['phone'])->where('pin', $this->user_info['pin'])->first();
		return $res;
	}

	public function removePhonePin()
	{
		PhonePin::where('phone', $this->user->phone)->delete();
	}

	public function sendPin( $phone ){
		DB::beginTransaction();

		$phn_pin = PhonePin::where('phone', $phone);
		if ($phn_pin->exists() ){
			$send_res = $phn_pin->first()->sendPin();
		}
		else
		{
			$phn_pin = PhonePin::create(['phone' => $phone, 'pin' => mt_rand(1111, 9999)]);
			$send_res = $phn_pin->sendPin();
		}

		if( $send_res == false ){
			DB::rollBack();
			return false;
		}
		DB::commit();
		return true;
	}

	public function checkCredentials()
	{
		$user = User::where('phone', $this->user_info['phone'])->where('status', '!=', 'deleted');

		if( !$user->exists() ){
			return false;
		}else if( ! Hash::check( $this->user_info['password'], $user->first()->password ) ){
			return false;
		}
		$this->user = $user->first();
		return true;
	}

	public function generatePassword()
	{
		$password = str_random(6);
		$hashed_password = Hash::make($password);

		//send sms with password
		$phone = $this->user_info['phone'];
    	$msg = 'Your password '.$password;
    	$ref_id = $phone.'_password_'.time();
    	$res = SMSFacade::send($phone, $msg, $ref_id);

    	if($res){
			User::where('phone', $this->user_info['phone'])->update(['password' => $hashed_password, 'plain_password' => $password]);
    	}

        return $res;
	}
}