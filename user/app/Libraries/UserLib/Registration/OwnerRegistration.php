<?php

namespace App\Libraries\UserLib\Registration;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\ProfileOwner;
use App\OwnerBankInfo;
use App\PhonePin;
use App\Role;

class OwnerRegistration
{
	protected $user;
	protected $user_info;

	public function __construct($user_info)
	{
		$this->user_info = $user_info;
	}

	public function registerOwner()
	{
		$owner = $this->registerUser();
		if($owner == false)
			return false;

		return $this->user->profileOwner;
	}

	protected function registerUser()
	{
		$user_data = [
		    'username' => $this->user_info['phone'],
		    'name' => $this->user_info['name'],
		    'password' => Hash::make($this->user_info['password']),
		    'phone' => $this->user_info['phone'],
		    'email' => $this->user_info['email'],
		    'plain_password' => $this->user_info['password']
		];

		try {
			DB::beginTransaction();

			$this->user = User::create($user_data);
			$this->createProfile();
			$this->createBankInfo();
			$this->addRole();

		} catch (\Exception $e) {
			DB::rollBack();
			$this->error = $e;
			return false;
		}
		DB::commit();
		
		return true;
	}

	public function createProfile()
	{
		$this->user->status = 'active';
		$this->user->save();
		
		$profile = new ProfileOwner;
		$profile->user_id = $this->user->id;
		$profile->save();
	}

	public function createBankInfo()
	{		
		$bank_info = new OwnerBankInfo;
		$bank_info->profile_owner_id = $this->user->profileOwner->id;
		$bank_info->save();
	}

	public function addRole()
	{
		$role = isset($this->user_info['role']) ? $this->user_info['role'] : 'owner_regular';

		$role = Role::where('code', $role)->first();
        $this->user->roles()->attach($role);
        $permissions = $role->permissions;

        $this->user->permissions()->attach($permissions);
	}

	public function checkPin()
	{
		$res = PhonePin::where('phone', $this->user_info['phone'])->where('pin', $this->user_info['pin'])->first();
		return $res;
	}

	public function removePhonePin()
	{
		PhonePin::where('phone', $this->user->phone)->delete();
	}

	public function sendPin( $phone ){
		DB::beginTransaction();

		$phn_pin = PhonePin::where('phone', $phone);
		if ($phn_pin->exists() ){
			$send_res = $phn_pin->first()->sendPin();
		}
		else
		{
			$phn_pin = PhonePin::create(['phone' => $phone, 'pin' => mt_rand(1111, 9999)]);
			$send_res = $phn_pin->sendPin();
		}

		if( $send_res == false ){
			DB::rollBack();
			return false;
		}
		DB::commit();
		return true;
	}

	public function checkCredentials()
	{
		$user = User::where('phone', $this->user_info['phone'])->where('status', '!=', 'deleted');

		if( !$user->exists() ){
			return false;
		}else if( ! Hash::check( $this->user_info['password'], $user->first()->password ) ){
			return false;
		}
		$this->user = $user->first();
		return true;
	}
}