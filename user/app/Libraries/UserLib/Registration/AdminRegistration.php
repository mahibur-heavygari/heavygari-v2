<?php

namespace App\Libraries\UserLib\Registration;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\ProfileAdmin;
use App\Role;
use App\User;

class AdminRegistration
{
	protected $user;
	protected $user_info;

	public function __construct($user_info)
	{
		$this->user_info = $user_info;
	}

	public function registerAdmin()
	{
		$admin = $this->registerUser();
		return $this->user->profileAdmin;
	}

	protected function registerUser()
	{
		$user_data = [
		    'username' => $this->user_info['email'],
		    'name' => $this->user_info['name'],
		    'password' => Hash::make($this->user_info['password']),
		    'phone' => $this->user_info['phone'],
		    'email' => $this->user_info['email'],
		    'plain_password' => $this->user_info['password']
		];

		try {

			$this->user = User::create($user_data);
			$this->createProfile();
			$this->addRole();

		} catch (\Exception $e) {

			$this->error = $e;
			return false;
		}
	}

	public function createProfile()
	{
		$this->user->status = 'active';
		$this->user->save();
		
		$profile = new ProfileAdmin;
		$profile->user_id = $this->user->id;
		$profile->profile_complete = 'yes';
		$profile->about = isset($this->user_info['about']) ? $this->user_info['about'] : NULL;
		$profile->nid_number = isset($this->user_info['nid_number']) ? $this->user_info['nid_number'] : NULL;
		$profile->save();
	}

	public function addRole()
	{
		$role = Role::where('code', $this->user_info['role'])->first();
        $this->user->roles()->attach($role);
        $permissions = $role->permissions;

        $this->user->permissions()->attach($permissions);
	}
}