<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileAdmin extends Model
{
    protected $table='base_profile_admins';
    
    protected $guarded = [];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function getApiModel()
    {
        return [
            'id' => $this->id,
            'username' => $this->user->username,
            'name' => $this->user->name,
            'phone' => $this->user->phone,
            'email' => $this->user->email,
            'status' => $this->user->status,
            'last_login' => $this->last_login,
            'about' => $this->about,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
