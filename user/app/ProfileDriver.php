<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileDriver extends Model
{
    protected $table='base_profile_drivers';
}
