<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OwnerBankInfo extends Model
{    
    protected $table = 'base_owner_bank_info';
    protected $guarded = [];
}
