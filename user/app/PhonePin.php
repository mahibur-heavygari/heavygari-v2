<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Libraries\SMSLib\SMSFacade;

class PhonePin extends Model
{
    protected $table='base_phone_pins';
    
	protected $fillable = [
        'phone', 'pin'
    ];

    public function sendPin()
    {
    	$phone = $this->phone;
    	$msg = 'Your pin '.$this->pin;
    	$ref_id = $phone.'_pin_'.time();

    	$res = SMSFacade::send($phone, $msg, $ref_id);
    	return $res;
    }
}
