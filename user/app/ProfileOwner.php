<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use URL;

class ProfileOwner extends Model
{
    protected $table='base_profile_owners';
    
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function bankInfo()
    {
        return $this->hasOne('App\OwnerBankInfo');
    }

    public function completeProfile($request)
    {
        $this->update($request->only( 'company_name', 'ownership_card_number', 'nid_number', 'address', 'about'));
        $this->profile_complete = 'yes';
        $this->save();
        return $this;
    }

    public function updateProfile($request)
    {
        $this->update($request->only('reference_number', 'company_name', 'ownership_card_number', 'address', 'about', 'nid_number'));

        $this->user->update($request->only('name', 'email'));

        $this->bankInfo->update($request->only('bank_name', 'bank_branch', 'account_number'));
    }

    public function getApiModel()
    {
        $photo = isset($this->photo) ? URL::to($this->photo) : URL::to(env('default_avatar'));
        $thumb_photo = isset($this->thumb_photo) ? URL::to($this->thumb_photo) : NULL;
        $ownership_card_photo = isset($this->ownership_card_photo) ? URL::to($this->ownership_card_photo) : NULL;
        $nid_photo = isset($this->nid_photo) ? URL::to($this->nid_photo) : NULL;

        return [
            'id' => $this->id,
            'username' => $this->user->username,
            'name' => $this->user->name,
            'phone' => $this->user->phone,
            'email' => $this->user->email,
            'status' => $this->user->status,
            //'last_login' => $this->last_login,
            'reference_number' => $this->reference_number,
            //'corporate' => $this->corporate,
            'without_app' => $this->without_app,
            'profile_complete' => $this->profile_complete,
            'company_name' => $this->company_name,
            'photo' => $photo,
            'thumb_photo' => $thumb_photo,
            'ownership_card_number' => $this->ownership_card_number,
            'ownership_card_photo' => $ownership_card_photo,
            'nid_number' => $this->nid_number,
            'nid_photo' => $nid_photo,
            'address' => $this->address,
            'about' => $this->about,
            'bank_name' => $this->bankInfo->bank_name,
            'bank_branch' => $this->bankInfo->bank_branch,
            'account_number' => $this->bankInfo->account_number,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}