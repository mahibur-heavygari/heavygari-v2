<?php
namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Libraries\UserLib\Registration\CustomerRegistration;
use App\Http\Controllers\ApiController;
use App\User;

class RegistrationController extends ApiController
{
	public function checkPhone( Request $request ){

		$phone = (string ) $request->phone;
		$user = User::where('phone', $request->phone)->first();
		
		if (is_null($user)) {
			
			$registration = new CustomerRegistration($request->all());
			$sent = $registration->sendPin( $phone );
			if($sent == false)
				return $this->respondErrorInDetails('Error occured. Pin not sent', []);

            return $this->apiResponse([
                'phone' => $phone,
                'is_pin_sent' => true,
                'already_has_account' => false
	        ]);
        }

        if($user->profileCustomer){
        	return $this->respondErrorInDetails('The phone number has already been taken', ['message'=>'Please correct the form', 'type'=>'phone_already_taken']);
        }
		
		return $this->apiResponse([
            'phone' => $phone,
            'is_pin_sent' => false,
            'already_has_account' => true
        ]);
	}

	public function matchPin ( Request $request )
	{
		$registration = new CustomerRegistration($request->all());
        $res = $registration->checkPin();

        if(is_null($res))
        	return $this->respondErrorInDetails('pin did not match', []);

		return $this->apiResponse([
            'message' => 'pin matched',
            'user_complete' => false,
            'profile_complete' => false
        ]);
	}

	public function register ( Request $request )
	{
        $user = User::where('email', $request->email)->orWhere('phone', $request->phone);
        if ($user->exists()){
            return $this->respondError('Phone or email already exists');
        }
		$registration = new CustomerRegistration($request->all());
        $customer = $registration->registerCustomer();

        if ($customer) {
        	//$registration->removePhonePin();
            return $this->apiResponse([
                'message' => 'Successfully Registered',
                'customer_id' => $customer->id
            ]);
        }else {
            return $this->respondServerError();
        }
	}

	public function matchPassword ( Request $request )
	{
		$registration = new CustomerRegistration($request->all());

        $res = $registration->checkCredentials();

        if($res == false)
        	return $this->respondErrorInDetails('Sorry! phone or password did not match', ['message' => 'Sorry! phone or password did not match.', 'type' => 'password_mismatch_at_reg']);

        $registration->addRole();

        $registration->createProfile();

		return $this->apiResponse([
            'message' => 'password matched'
        ]);
	}

    public function sendPinAtForgetPassword( Request $request ){

        $phone = (string ) $request->phone;
        $user = User::where('phone', $request->phone)->first();
        
        if (is_null($user)) {
            
            return $this->respondErrorInDetails('The phone number not found', ['message'=>'The phone number not found', 'type'=>'unverified_phone']);
        }
        
        $registration = new CustomerRegistration($request->all());
        $sent = $registration->sendPin( $phone );
        if($sent == false)
            return $this->respondErrorInDetails('Error occured. Pin not sent', []);

        return $this->apiResponse([
            'phone' => $phone,
            'is_pin_sent' => true,
        ]);
    }

    public function matchPinAtForgetPassword( Request $request ){

        $phone = (string ) $request->phone;
        $user = User::where('phone', $request->phone)->first();
        
        if (is_null($user)) {
            
            return $this->respondErrorInDetails('The phone number not found', ['message'=>'The phone number not found', 'type'=>'unverified_phone']);
        }
        
        $registration = new CustomerRegistration($request->all());
        $res = $registration->checkPin();

        if(is_null($res))
            return $this->respondErrorInDetails('pin did not match', []);

        $res = $registration->generatePassword();

        if($res == false)
            return $this->respondErrorInDetails('Error occured. Password not sent', []);

        return $this->apiResponse([
            'phone' => $phone,
            'password_sent' => $res,
        ]);
    }
}
