<?php
namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\ProfileAdmin;
use App\User;
use App\Role;
use App\Libraries\UserLib\Registration\AdminRegistration;

class AdminUserController extends ApiController
{
	public function show( $id ){
		$admin = ProfileAdmin::where('id', $id)->first();
		if (!$admin) {
            return $this->respondNotFound();
        }		
        return $this->reform($admin);
	}
	
	public function store(Request $request){	
		$role = Role::where('code', $request->role)->where('panel', 'admin');
		if (!$role->exists()){
            return $this->respondNotFound('Role not found');
        }		

        $user = User::where('username', $request->email)->orWhere('phone', $request->phone);
    	if ($user->exists()){
            return $this->respondError('Phone or email already exists');
        }
		$registration = new AdminRegistration($request->all());
		$admin = $registration->registerAdmin();

        return $this->apiResponse([
            'message' => 'Successfully Registered',
            'admin_id' => $admin->id
        ]);
	}
}