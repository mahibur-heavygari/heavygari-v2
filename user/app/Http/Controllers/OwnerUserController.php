<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Libraries\UserLib\FileUpload;
use App\ProfileOwner;
use App\OwnerBankInfo;
use App\User;
use App\Role;
use Validator;
use App\Libraries\UserLib\Registration\OwnerRegistration;

class OwnerUserController extends ApiController
{
	public function show( $id )
	{
		$owner = ProfileOwner::where('id', $id)->first();
		if (!$owner) {
            return $this->respondNotFound();
        }		
        return $this->reform($owner);
	}

	public function store( Request $request )
	{
		$role = Role::where('code', $request->role)->where('panel', 'owner');
		if (!$role->exists()){
            return $this->respondNotFound('Role not found');
        }

        $user = User::where('email', $request->email)->orWhere('phone', $request->phone);
    	if ($user->exists()){
            return $this->respondError('Phone or email already exists');
        }

        $registration = new OwnerRegistration( $request->all() );

		$owner = $registration->registerOwner(); 
        if(!$owner){
        	return $this->respondServerError();
        }

		$owner->updateProfile($request);
		$owner->profile_complete = 'yes';
		$owner->save();

	    return $this->apiResponse([
            'message' => 'Successfully Registered',
            'owner_id' => $owner->id
        ]);
	}

	public function completeProfile( Request $request, $id )
	{
		$owner = ProfileOwner::find( $id );
		if (!$owner) {
            return $this->respondNotFound();
        }

		$owner->updateProfile($request);
		$owner->completeProfile($request);

		return $this->apiResponse([
            'message' => 'Successfully completed'
        ]);
	}

	public function updateProfile( Request $request, $id )
	{
		$owner = ProfileOwner::find( $id );
		if (!$owner) {
            return $this->respondNotFound();
        }

        $rules =  [
            'email'=>'unique:users,email,'.$owner->user->id
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $owner->updateProfile($request);

		return $this->apiResponse([
            'message' => 'Successfully updated'
        ]);
	}

	public function uploadPhoto( Request $request, $id ){
		
		$storagePath = '/avatars/owners';
        $res = FileUpload::uploader( $request, $storagePath );

		$owner = ProfileOwner::where('id', $id);
		if(!is_null($owner->first()->photo)){
			$previous = $destinationPath = base_path('public'). $owner->first()->photo;
			if (file_exists($previous))
				unlink($previous);
		}
		$owner->update(['photo' => $res['path']]);

        return $this->apiResponse($res);
	}

	public function uploadOwnershipCardPhoto( Request $request, $id ){
		
		$storagePath = '/ownership-cards/owners';
        $res = FileUpload::uploader( $request, $storagePath );

		$owner = ProfileOwner::where('id', $id);
		if(!is_null($owner->first()->ownership_card_photo)){
			$previous = $destinationPath = base_path('public'). $owner->first()->ownership_card_photo;
			if (file_exists($previous))
				unlink($previous);
		}
		$owner->update(['ownership_card_photo' => $res['path']]);

        return $this->apiResponse($res);
	}

	public function uploadNidPhoto( Request $request, $id ){
		
		$storagePath = '/national_ids/owners';
        $res = FileUpload::uploader( $request, $storagePath );

		$owner = ProfileOwner::where('id', $id);
		if(!is_null($owner->first()->nid_photo)){
			$previous = $destinationPath = base_path('public'). $owner->first()->nid_photo;
			if (file_exists($previous))
				unlink($previous);
		}
		$owner->update(['nid_photo' => $res['path']]);

        return $this->apiResponse($res);
	}
}