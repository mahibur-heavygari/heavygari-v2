<?php
namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Libraries\UserLib\Registration\OwnerRegistration;
use App\Http\Controllers\ApiController;
use App\User;

class RegistrationController extends ApiController
{
	public function checkPhone( Request $request ){

		$phone = (string ) $request->phone;
		$user = User::where('phone', $request->phone)->first();
		
		if (is_null($user)) {
			
			$registration = new OwnerRegistration($request->all());
			$sent = $registration->sendPin( $phone );
			if($sent == false)
				return $this->respondErrorInDetails('Error occured. Pin not sent', []);

            return $this->apiResponse([
                'phone' => $phone,
                'is_pin_sent' => true,
                'already_has_account' => false
	        ]);
        }

        if($user->profileOwner){
        	return $this->respondErrorInDetails('The phone number has already been taken', ['message'=>'Please correct the form', 'type'=>'phone_already_taken']);
        }
		
		return $this->apiResponse([
            'phone' => $phone,
            'is_pin_sent' => false,
            'already_has_account' => true
        ]);
	}

	public function matchPin ( Request $request )
	{
		$registration = new OwnerRegistration($request->all());
        $res = $registration->checkPin();

        if(is_null($res))
        	return $this->respondErrorInDetails('pin did not match', []);

		return $this->apiResponse([
            'message' => 'pin matched',
            'user_complete' => false,
            'profile_complete' => false
        ]);
	}

	public function register ( Request $request )
	{
        $user = User::where('email', $request->email)->orWhere('phone', $request->phone);
        if ($user->exists()){
            return $this->respondError('Phone or email already exists');
        }
        
		$registration = new OwnerRegistration($request->all());
        $owner = $registration->registerOwner();

        if ($owner) {
        	$registration->removePhonePin();
            return $this->apiResponse([
                'message' => 'Successfully Registered',
                'owner_id' => $owner->id
            ]);
        }else {
            return $this->respondServerError();
        }
	}

	public function matchPassword ( Request $request )
	{
		$registration = new OwnerRegistration($request->all());
        $res = $registration->checkCredentials();

        if($res == false)
        	return $this->respondErrorInDetails('Sorry! password did not match', ['message' => 'Sorry! password did not match.', 'type' => 'password_mismatch_at_reg']);

        $registration->addRole();

        $registration->createProfile();

        $registration->createBankInfo();

		return $this->apiResponse([
            'message' => 'password matched'
        ]);
	}

}
