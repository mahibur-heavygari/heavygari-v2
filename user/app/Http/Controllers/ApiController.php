<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response as IlluminateResponse;
use App\Enumarations\ApiErrorCodes;

class ApiController extends Controller
{
    protected $status_code = 200;

    public function getStatusCode()
    {
        return $this->status_code;
    }
    
    public function setStatusCode($status_code)
    {
        $this->status_code = $status_code;

        return $this;
    }

    public function apiResponse($data, $headers = [])
    {
        return response()->json($data, $this->getStatusCode(), $headers);
    }

    public function apiErrorResponse($message, $error_details = false)
    {
        if ($error_details) {

            $error['message'] = isset($error_details['message']) ? $error_details['message'] :  $message;

            $error['type'] = $error_details['type'];

            if ($error_details['type'] == 'validation_error') {
                $error['error_details']['error_code'] = ApiErrorCodes::$VALIDATION_ERROR;
                $error['error_details']['error_title'] = 'Validation error';
                $error['error_details']['error_feild'] = null;
                $error['error_details']['validation_errors'] = $error_details['validation_errors'];
            }else if ($error_details['type'] == 'phone_already_taken') {
                $error['error_details']['error_code'] = ApiErrorCodes::$PHONE_ALREADY_TAKEN;
                $error['error_details']['error_title'] = 'Phone already taken';
                $error['error_details']['error_feild'] = null;
                $error['error_details']['error_message'] = $message;
            }else if ($error_details['type'] == 'password_mismatch_at_reg') {
                $error['error_details']['error_code'] = ApiErrorCodes::$PASSWORD_MISMATCH_AT_REG;
                $error['error_details']['error_title'] = 'Phone or password mismatch';
                $error['error_details']['error_feild'] = null;
            }else if ($error_details['type'] == 'unverified_phone') {
                $error['error_details']['error_code'] = ApiErrorCodes::$UNVERIFIED_PHONE;
                $error['error_details']['error_title'] = 'Phone not found';
                $error['error_details']['error_feild'] = null;
            }
            unset($error['error_details']['type']);
        } else {
            $error['message'] = $message;
            $error['type'] = null;
            $error['error_details'] = null;
        }

        return $this->apiResponse([
            'error' => $error
        ]);
    }
    
    public function respondError($message = 'Bad Request')
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST)->apiErrorResponse($message);
    }

    public function respondErrorInDetails($message, $error_details)
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST)->apiErrorResponse($message, $error_details);
    }

    public function respondNotFound($message = 'Not Found')
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND)->apiErrorResponse($message);
    }

    public function respondServerError($message = 'Internal Server Error')
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR)->apiErrorResponse($message);
    }

    public function respondValidationError($fields, $message = 'Please correct the errors in the form!')
    {
        $error_details = [
            'type' => 'validation_error',
            'validation_errors' => $fields
        ];

        return $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST)->apiErrorResponse($message, $error_details);
    }
    
    public function reform($item)
    {
        return $item->getApiModel();
    }


    public function transformErrorMessage($validator)
    {
        $validation_errors = $validator->errors()->messages();

        $error_messages = [];
        foreach($validation_errors as $key => $msg) {
            $error_messages[] = [
                'field' => $key,
                'errors' => $msg
            ];
        }

        return $error_messages;
    }
}
