<?php
namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Libraries\UserLib\FileUpload;
use Illuminate\Http\Request;
use App\ProfileCustomer;
use App\User;
use App\Role;
use Validator;
use App\Libraries\UserLib\Registration\CustomerRegistration;

class CustomerUserController extends ApiController
{
	public function show( $id ){
		$customer = ProfileCustomer::where('id', $id)->first();
		if (!$customer) {
            return $this->respondNotFound();
        }		
        return $this->reform($customer);
	}

	public function store( Request $request ){
		$role = Role::where('code', $request->role)->where('panel', 'customer');
		if (!$role->exists()){
            return $this->respondNotFound('Role not found');
        }	

        $user = User::where('email', $request->email)->orWhere('phone', $request->phone);
    	if ($user->exists()){
            return $this->respondError('Phone or email already exists');
        }

        $registration = new CustomerRegistration($request->all());
		$customer = $registration->registerCustomer(); 
        $customer->updateProfile($request);

		return $this->apiResponse([
            'message' => 'Successfully Registered',
            'customer_id' => $customer->id
        ]);
	}

	public function updateProfile( Request $request, $id )
	{
		$customer = ProfileCustomer::find( $id );
		if (!$customer) {
            return $this->respondNotFound();
        }

        $rules =  [
            'email'=>'unique:users,email,'.$customer->user->id
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation_errors = $this->transformErrorMessage($validator);
            return $this->respondValidationError($validation_errors);
        }

        $customer->updateProfile($request);

		return $this->apiResponse([
            'message' => 'Successfully updated'
        ]);
	}

	public function uploadPhoto( Request $request, $id ){
		
		$storagePath = '/avatars/customers';
        $res = FileUpload::uploader( $request, $storagePath );

		$customer = ProfileCustomer::where('id', $id);
		if(!is_null($customer->first()->photo)){
			$previous = $destinationPath = base_path('public'). $customer->first()->photo;
			if (file_exists($previous))
				unlink($previous);
		}
		$customer->update(['photo' => $res['path']]);

        return $this->apiResponse($res);
	}
}