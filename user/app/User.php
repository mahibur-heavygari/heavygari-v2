<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password', 'name', 'email', 'phone', 'status', 'plain_password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function roles() {
        return $this->belongsToMany(Role::class);
    }

    public function permissions() {
        return $this->belongsToMany(Permission::class);
    }

    public function scopeBlocked($query)
    {
        return $query->where('status', 'blocked');
    }

    public function profileAdmin()
    {
        return $this->hasOne('App\ProfileAdmin');
    }

    public function profileCustomer()
    {
        return $this->hasOne('App\ProfileCustomer');
    }

    public function profileOwner()
    {
        return $this->hasOne('App\ProfileOwner');
    }

    public function profileDriver()
    {
        return $this->hasOne('App\ProfileDriver');
    }

    public function profileVehicle()
    {
        return $this->hasOne('App\ProfileVehicle');
    }

    public function getApiModel()
    {
        return [
            'username' => $this->username,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'status' => $this->status,
            'about' => $this->about,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
