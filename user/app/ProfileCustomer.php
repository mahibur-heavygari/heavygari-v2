<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use URL;

class ProfileCustomer extends Model
{
    protected $table='base_profile_customers';
    
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function updateProfile($request)
    {
        $this->update($request->only('address', 'about', 'nid_number'));

        $this->user->update($request->only('name', 'email'));

        //Both users and base_profile_customers tables will be updated
        $this->touch();
        $this->user->touch();
    }

    public function getApiModel()
    {
        $photo = isset($this->photo) ? URL::to($this->photo) : URL::to(env('default_avatar'));
        return [
            'id' => $this->id,
            'username' => $this->user->username,
            'name' => $this->user->name,
            'phone' => $this->user->phone,
            'email' => $this->user->email,
            'status' => $this->user->status,
            //'last_login' => $this->last_login,
            'corporate' => $this->corporate,
            'photo' => $photo,
            'about' => $this->about,
            'address' => $this->address,
            'nid_number' => $this->nid_number,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
