<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('admins/{id}', 'AdminUserController@show');
$router->post('admins/store', 'AdminUserController@store');

$router->get('customers/{id}', 'CustomerUserController@show');
$router->post('customers/store', 'CustomerUserController@store');
$router->post('customers/{id}/profile-update', 'CustomerUserController@updateProfile');
$router->post('customers/{id}/upload-photo', 'CustomerUserController@uploadPhoto');


$router->get('owners/{id}', 'OwnerUserController@show');
$router->post('owners/store', 'OwnerUserController@store');
$router->post('owners/{id}/profile-complete', 'OwnerUserController@completeProfile');
$router->post('owners/{id}/profile-update', 'OwnerUserController@updateProfile');

$router->post('owners/{id}/upload-photo', 'OwnerUserController@uploadPhoto');
$router->post('owners/{id}/upload-ownership-card-photo', 'OwnerUserController@uploadOwnershipCardPhoto');
$router->post('owners/{id}/upload-nid-photo', 'OwnerUserController@uploadNidPhoto');


$router->group(['prefix' => 'customer'], function () use ($router) {
    $router->post('/check-phone', 'Customer\RegistrationController@checkPhone');
    $router->post('/register', 'Customer\RegistrationController@register');
    $router->post('/match-pin', 'Customer\RegistrationController@matchPin');
    $router->post('/match-password', 'Customer\RegistrationController@matchPassword');
    $router->post('/forget-password/send-pin', 'Customer\RegistrationController@sendPinAtForgetPassword');
    $router->post('/forget-password/match-pin', 'Customer\RegistrationController@matchPinAtForgetPassword');
});


$router->group(['prefix' => 'owner'], function () use ($router) {
    $router->post('/check-phone', 'Owner\RegistrationController@checkPhone');
    $router->post('/register', 'Owner\RegistrationController@register');
    $router->post('/match-pin', 'Owner\RegistrationController@matchPin');
    $router->post('/match-password', 'Owner\RegistrationController@matchPassword');
});

$router->get('/', function() {
    return "hello lumen";
});
$router->get('/test', function() {
    return "hello lumen test";
});

